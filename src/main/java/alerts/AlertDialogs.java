package alerts;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;

import java.util.Optional;

/**
 * AlertDialogs class is responsible for shows error and confirmation dialog to the user interface.
 * Before showing are set window attributes. This is based on Alert class.
 * @see Alert
 */
public class AlertDialogs {

    private Alert errorAlertDialog =  new Alert(Alert.AlertType.ERROR);
    private Alert confirmationDialog = new Alert(Alert.AlertType.CONFIRMATION);

    /**
     * Shows the confirmation dialog to the user and wait for response.
     * @param title         the string of characters who will be displayed in the title of dialog
     * @param header        the string of characters who will be displayed as header of dialog
     * @param context       the communicate who will be shown to the users
     * @return              <code>true</code> if user accept;
     *                      <code>false</code> if user choose other option.
     * @see Alert#showAndWait()
     */
    public boolean setConfirmationDialog(String title, String header, String context){
        confirmationDialog.setTitle(title);
        confirmationDialog.setHeaderText(header);
        confirmationDialog.setContentText(context);

        ButtonType buttonTypeYes = new ButtonType("Yes");
        ButtonType buttonTypeNo = new ButtonType("No");

        confirmationDialog.getButtonTypes().setAll(buttonTypeYes, buttonTypeNo);

        Optional<ButtonType> result = confirmationDialog.showAndWait();
        if (result.get() == buttonTypeYes){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Sets error dialog attributes.
     * @param title     the window title
     * @param header    the string of characters before message text
     * @param context   the error message who will be displayed to user
     */
    public void setErrorDialog(String title, String header, String context){
        errorAlertDialog.setTitle(title);
        errorAlertDialog.setHeaderText(header);
        errorAlertDialog.setContentText(context);
    }

    /**
     * Shows user error dialog with set attributes that we can assign in function listed below.
     * @see AlertDialogs#setErrorDialog(String, String, String)
     */
    public void showErrorDialog(){
        errorAlertDialog.showAndWait();
    }
}
