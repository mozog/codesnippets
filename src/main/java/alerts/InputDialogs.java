package alerts;

import javafx.scene.control.TextInputDialog;

import java.util.Optional;

/**
 * The InputDialogs class is responsible for generate window with text field who will be displayed to the user interface.
 * @see TextInputDialog
 */
public class InputDialogs {

    private TextInputDialog inputDialog = new TextInputDialog();

    /**
     * Sets input dialog attributes which are describing below.
     * @param title         the window title
     * @param headerText    the string of character displayed above communicate to the user
     * @param contentText   the content of communicate
     */
    public void setInputDialogInfo(String title, String headerText, String contentText){
        inputDialog.setTitle(title);
        inputDialog.setHeaderText(headerText);
        inputDialog.setContentText(contentText);
    }

    /**
     *  Shows the input dialog on the user interface and waiting for the response.
     * @return      the object who will have user response
     * @see Optional
     * @see TextInputDialog#showAndWait()
     */
    public Optional<String> getResultFromInputDialog(){
        return inputDialog.showAndWait();
    }
}
