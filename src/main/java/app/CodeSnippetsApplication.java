package app;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import utils.HibernateSessionFactory;

public class CodeSnippetsApplication extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/styles/MainWindow/MainWindow.fxml"));
        Scene scene = new Scene(root, 1280, 780);
        scene.getStylesheets().add(getClass().getResource("/styles/MainWindow/MainWindow.css").toExternalForm());
        primaryStage.setTitle("CodeSnippets");
        primaryStage.setMinWidth(1024);
        primaryStage.setMinHeight(780);
        primaryStage.setScene(scene);
        primaryStage.getIcons().add(new Image("/icons/logo.jpg"));
        primaryStage.setOnCloseRequest(event -> HibernateSessionFactory.getSessionFactory().close());
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
