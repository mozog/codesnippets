package filters;

import java.util.Date;
import java.util.List;

/**
 * Storage attribute used in filter process. The parameters will fill when user choose option on the interface and approve operation.
 */
public class Filters {

    private String sortBy;
    private String typeSorting;
    private List<String> programmingLanguage;
    private List<String> typeSnippet;
    private List<String> addedFrom;
    private Date addedAfter, addedBefore;
    private boolean favorite;

    /**
     * Create object with all attribute.
     * @param sortBy                the attribute after which the code snippet will be sorted
     * @param typeSorting           the type of sorting variant (e.g: ascending, descending)
     * @param programmingLanguage   the language in which the code snippet was written
     * @param typeSnippet           the type of snippet (e.g: method, class)
     * @param addedFrom             the location where code snippet was created (e.g: home, office)
     * @param addedAfter            the parameter restrictive search results by the creation date of snippet
     * @param addedBefore           the parameter restrictive search results by the creation date of snippet
     * @param favorite              the attribute describing the subjective opinion of the user about snippet
     *                              <code>true</code> if code snippet is marked as favorite;
     *                              <code>false</code> if snippet is not marked as favorite.
     */
    public Filters(String sortBy, String typeSorting, List<String> programmingLanguage, List<String> typeSnippet, List<String> addedFrom, Date addedAfter, Date addedBefore, boolean favorite){
        this.sortBy = sortBy;
        this.typeSorting = typeSorting;
        this.programmingLanguage = programmingLanguage;
        this.typeSnippet = typeSnippet;
        this.addedFrom = addedFrom;
        this.addedAfter = addedAfter;
        this.addedBefore = addedBefore;
        this.favorite = favorite;
    }

    public String getSortBy() {
        return sortBy;
    }

    public String getTypeSorting() {
        return typeSorting;
    }

    public List<String> getProgrammingLanguage() {
        return programmingLanguage;
    }

    public List<String> getTypeSnippet() {
        return typeSnippet;
    }

    public List<String> getAddedFrom() {
        return addedFrom;
    }

    public Date getAddedAfter() {
        return addedAfter;
    }

    public Date getAddedBefore() {
        return addedBefore;
    }

    public boolean isFavorite() {
        return favorite;
    }
}
