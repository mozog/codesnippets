package filters;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import models.CodeSnippet;

import java.util.*;

/**
 * Applies filters to set of code snippets. Parameters filter is storage in Filters object.
 * @see Filters
 */
public class FiltersService {

    private ObservableList<Object> setSnippetsWithFilters = FXCollections.observableArrayList();
    private List<Object> tmpList = new ArrayList<>();
    private Filters filters;

    /**
     * Apply filters in searched code snippet set. Filter parameters are in Filters object.
     * @param setSnippetsWithoutFilters     the set of code snippet which are search results
     * @param filters                       the object who has filters parameters to apply
     * @return                              sorted and filtered list
     */
    public ObservableList<Object> applyFilters(ObservableSet<Object> setSnippetsWithoutFilters, Filters filters){
        this.filters = filters;
        setSnippetsWithFilters.clear();
        List<Object> resultsList = new ArrayList<>(setSnippetsWithoutFilters);
        if(filters.getProgrammingLanguage().size()!=0) {
            resultsList = filterByProgrammingLanguage(resultsList);
        }
        if(filters.getTypeSnippet().size()!=0) {
            resultsList = filterByTypeSnippet(resultsList);
        }
        if (filters.getAddedFrom().size()!=0) {
            resultsList = filterByAddedFrom(resultsList);
        }
        resultsList = filterByDate(resultsList);
        resultsList = filterByFavorite(resultsList);
        resultsList = sortList(resultsList);

        setSnippetsWithFilters.addAll(resultsList);

        return setSnippetsWithFilters;
    }

    /**
     * Sort input list by one of three attribute. None - not sorting, Name - sort by name. If none of this option is selected,
     * algorithm sort by creation date of code snippet.
     * @param resultsList   the list with others snippet ready to sorting
     * @return              if sorting parameter was different from none return sorted list.
     * @see FiltersService#sortListByName(List)
     * @see FiltersService#sortListByDate(List)
     */
    private List<Object> sortList(List<Object> resultsList) {
        tmpList.clear();
        if(!filters.getSortBy().equals("None")){
            if(filters.getSortBy().equals("Name")){
                tmpList = sortListByName(resultsList);
            }
            else{
                tmpList = sortListByDate(resultsList);
            }
            return new ArrayList<>(tmpList);
        }
        else {
            return resultsList;
        }
    }

    /**
     * Sorts input list by creation date declared in parameters sorting. Can order by descending and ascending option. Object in list are CodeSnippet instance.
     * @param resultsList       input list object to sort, who will be CodeSnippet instance
     * @return                  sorted list by chosen order
     * @see Collections
     * @see Comparator
     */
    private List<Object> sortListByDate(List<Object> resultsList) {
        Collections.sort(resultsList, Comparator.comparing(o -> ((CodeSnippet) o).getAddedDate()));
        if(filters.getTypeSorting().equals("DSC")){
            Collections.reverse(resultsList);
        }
        return resultsList;
    }

    /**
     * Sorts input list by name object. Use Collections static method called sort who will sorting list. If option order is descending,
     * first sort object by ascending and next reverse the list.
     * @param resultsList       input list to sort with CodeSnippet object
     * @return                  sorted list
     * @see Collections#sort(List)
     * @see Collections#reverse(List)
     */
    private List<Object> sortListByName(List<Object> resultsList) {
        if(filters.getTypeSorting().equals("ASC")){
            Collections.sort(resultsList, new SortNameComparator());
        }
        else{
            Collections.sort(resultsList, new SortNameComparator());
            Collections.reverse(resultsList);
        }
        return resultsList;
    }

    /**
     * Filters input list by favorite attribute, chosen in filter pane. Filters object has got all parameters.
     * @param resultsList       the input list with not filtered objects
     * @return                  list with favorite or not favorite code snippets
     */
    private List<Object> filterByFavorite(List<Object> resultsList) {
        tmpList.clear();
        for(Object object:resultsList){
            if(object instanceof CodeSnippet){
                if(((CodeSnippet) object).isFavortie() == filters.isFavorite()){
                    tmpList.add(object);
                }
            }
        }
        return new ArrayList<>(tmpList);
    }

    /**
     * Check creation date in list objects and compare it with filter parameters.
     * @param resultsList       not filtered list
     * @return                  list with objects which creation contained between parameters in Filters object
     */
    private List<Object> filterByDate(List<Object> resultsList) {
        tmpList.clear();
        if(areCorrectDates()){
            for(Object object:resultsList){
                if(object instanceof CodeSnippet){
                    if(((CodeSnippet) object).getAddedDate().after(filters.getAddedAfter()) && ((CodeSnippet) object).getAddedDate().before(filters.getAddedBefore())){
                        tmpList.add(object);
                    }
                }
            }
            return new ArrayList<>(tmpList);
        }
        else {
            return resultsList;
        }
    }

    /**
     * Check the correctness of the dates in the Filters object.
     * @return  <true>if first date is before second date</true>
     *          <false>otherwise</false>
     */
    private boolean areCorrectDates() {
        if(filters.getAddedAfter() != null && filters.getAddedBefore()!=null){
            if(filters.getAddedAfter().before(filters.getAddedBefore())){
                return true;
            }
        }
        return false;
    }

    /**
     * Filter code snippet by create localization.
     * @param resultsList       list with snippets which have got all localization
     * @return                  filtered list
     */
    private List<Object> filterByAddedFrom(List<Object> resultsList) {
        tmpList.clear();
        for(Object object:resultsList){
            CodeSnippet codeSnippet = (CodeSnippet) object;
            if(object instanceof CodeSnippet){
                for(String addedFrom:filters.getAddedFrom()) {
                    if(codeSnippet.getAddedFrom()==null && addedFrom.equals("none")){
                        tmpList.add(object);
                        break;
                    }
                    if (codeSnippet.getAddedFrom()!=null && codeSnippet.getAddedFrom().equals(addedFrom)){
                        tmpList.add(object);
                        break;
                    }
                }
            }
        }
        return new ArrayList<>(tmpList);
    }

    /**
     * Filters list code snippets by type of snippet.
     * @param resultsList       list with snippets which have different types
     * @return                  filtered list with chosen types
     */
    private List<Object> filterByTypeSnippet(List<Object> resultsList) {
        tmpList.clear();
        for(Object object:resultsList){
            CodeSnippet codeSnippet = (CodeSnippet) object;
            if(object instanceof CodeSnippet){
                for(String snippetType:filters.getTypeSnippet()) {
                    if(codeSnippet.getType()==null && snippetType.equals("none")){
                        tmpList.add(object);
                        break;
                    }
                    if (codeSnippet.getType()!=null && codeSnippet.getType().equals(snippetType)){
                        tmpList.add(object);
                        break;
                    }
                }
            }
        }
        return new ArrayList<>(tmpList);
    }

    /**
     * Filters code snippet list by chosen programming languages.
     * @param resultsList   list with snippets ready to filtering
     * @return              filtered list by chosen languages
     */
    private List<Object> filterByProgrammingLanguage(List<Object> resultsList) {
        tmpList.clear();
        for(Object object: resultsList){
            if(object instanceof CodeSnippet){
                CodeSnippet codeSnippet = (CodeSnippet) object;
                for(String programmingLanguage:filters.getProgrammingLanguage()) {
                    if(codeSnippet.getProgrammingLanguage()==null && programmingLanguage.equals("none")){
                        tmpList.add(object);
                        break;
                    }
                    if (codeSnippet.getProgrammingLanguage()!=null && codeSnippet.getProgrammingLanguage().equals(programmingLanguage)){
                        tmpList.add(object);
                        break;
                    }
                }
            }
        }
        return new ArrayList<>(tmpList);
    }
}
