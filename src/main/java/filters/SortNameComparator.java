package filters;

import java.util.Comparator;

/**
 * Support sorting by name snippet process.
 * @see Comparator
 * @see Comparator#compare(Object, Object)
 */
public class SortNameComparator implements Comparator<Object> {

    @Override
    public int compare(Object o1, Object o2) {
        return o1.toString().toLowerCase().compareTo(o2.toString().toLowerCase());
    }

}
