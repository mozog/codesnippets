/**
 *  Provides class that support filters code snippet in search panel. Search panel is located on the right side of the main application window.
 *  But the filter pane itself is hidden in the bottom part of the abcve search panel.
 */
package filters;