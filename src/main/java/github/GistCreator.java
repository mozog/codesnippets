package github;

import models.CodeSnippet;
import org.kohsuke.github.GHGist;
import org.kohsuke.github.GHGistBuilder;

import java.io.IOException;

/**
 * Create GHGist object and send it to GitHub as gist.
 */
public class GistCreator {

    private GHGistBuilder ghGistBuilder;

    /**
     * Create GHGist from CodeSnippet object and send by create function.
     * @param codeSnippet       the code snippet who will be send to repository as gist
     * @return                  the created GHGist object who have information about gist (e.g ID on GitHub)
     * @see GHGistBuilder
     * @see GHGist
     */
    public GHGist createGist(CodeSnippet codeSnippet){
        GHGist ghGist = null;
        try {
            ghGistBuilder = GithubConnector.getGitHub().createGist().description(codeSnippet.getDescription());
            ghGistBuilder = ghGistBuilder.file(codeSnippet.getName(), codeSnippet.getContent());
            ghGist = ghGistBuilder.create();
            codeSnippet.setGistID(String.valueOf(ghGist.getHtmlUrl().getPath()).substring(1,ghGist.getHtmlUrl().getPath().length()));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ghGist;
    }

}
