package github;

import javafx.scene.image.Image;
import models.CodeSnippet;
import models.GitHubUser;
import org.kohsuke.github.GHUser;
import org.kohsuke.github.GitHub;
import java.io.IOException;

/**
 * Class responsible for manages connection with GitHub repository. Connect and disconnect user session. Manages user information.
 * @see GitHub
 * @see GitHubUser
 */
public class GithubConnector {

    private static GitHub gitHub;
    private static GHUser ghUser;
    private static GitHubUser gitHubUser;
    private static boolean loggedUser = false;

    /**
     * Set connection to GitHub. Get user info from service.
     * @param login         the login used in GitHub repository
     * @param oAuthToken     access key, generated on service, unique for each user
     * @throws IOException   throw if can't connect to repository
     */
    public static void createConnectionToGitHub(String login, String oAuthToken) throws IOException {
        gitHub = GitHub.connect(login, oAuthToken);
        ghUser = gitHub.getUser(login);
    }

    /**
     *
     * @return                  user avatar from GitHub
     * @throws IOException      where detect problems with connection
     */
    public static Image getAvatarImage() throws IOException {
        return new Image(ghUser.getAvatarUrl());
    }

    public static GitHub getGitHub(){
        return gitHub;
    }

    public static GHUser getGhUser() {
        return ghUser;
    }

    /**
     * Close connection by setting null in GitHub object.
     */
    public static void closeConnection() {
        gitHub =  null;
        ghUser = null;
        loggedUser = false;
    }

    public static GitHubUser getGitHubUser() {
        return gitHubUser;
    }

    public static void setGitHubUser(GitHubUser gitHubUser) {
        GithubConnector.gitHubUser = gitHubUser;
    }

    public static boolean isLoggedUser() {
        return loggedUser;
    }

    public static void setLoggedUser(boolean loggedUser) {
        GithubConnector.loggedUser = loggedUser;
    }
}
