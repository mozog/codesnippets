package github;

import alerts.AlertDialogs;
import gui.controllers.ControlPaneController;
import javafx.scene.control.TreeItem;
import models.Category;
import models.CodeSnippet;
import org.kohsuke.github.GHGist;
import org.kohsuke.github.GHGistFile;
import org.kohsuke.github.PagedIterable;
import services.SnippetServiceDB;
import utils.*;

import java.util.List;
import java.util.Map;

/**
 * Class responsible for upload and download code snippet from remote repository - GitHub.
 */
public class GithubServices {

    private ControlPaneController controlPaneController;
    private SnippetServiceDB snippetServiceDB = new SnippetServiceDB();
    private CodeSnippetCreator codeSnippetCreator;
    private GistCreator gistCreator = new GistCreator();

    /**
     * Set controller responsible for tools bar.
     * @param controlPaneController     controller class with GitHub button which sending action and wait for response
     * @see ControlPaneController
     */
    public GithubServices(ControlPaneController controlPaneController) {
        this.controlPaneController = controlPaneController;
        codeSnippetCreator = new CodeSnippetCreator(controlPaneController);
    }

    /**
     * Get list gists from user repository.
     * @see PagedIterable
     */
    public void pullGists() {
        PagedIterable<GHGist> pagedIterable = null;
        try {
            pagedIterable = GithubConnector.getGhUser().listGists();
            List<GHGist> listGists = pagedIterable.asList();
            loadPulledSnippet(listGists);
        } catch (Exception e) {
            AlertDialogs alertDialogs = new AlertDialogs();
            alertDialogs.setErrorDialog("Error!", "Pulled failed!", "Something gone wrong, check connection with GitHub.");
            alertDialogs.showErrorDialog();
        }
    }

    /**
     * Create code snippet model object from GHGist. Save loaded snippet to database.
     * @param listGists         users gist pulled from GitHub repository
     * @throws Exception        throws after created snippet have the same name with other
     */
    private void loadPulledSnippet(List<GHGist> listGists) throws Exception {
        Category githubCategory = createCategoryForGist();
        for (GHGist ghGist : listGists) {
            Map<String, GHGistFile> gistFiles = ghGist.getFiles();
            CodeSnippet newCodeSnippet = codeSnippetCreator.createOrFindInDBSnippet(ghGist, gistFiles, githubCategory);
            snippetServiceDB.saveSnippetToDB(newCodeSnippet);
        }
        controlPaneController.getTreeViewPaneController().refreshTreeView();
    }

    /**
     * Creates category with Github name for pulled gists.
     * @return      created category to storage pulled snippet
     */
    private Category createCategoryForGist() {
        Category githubCategory = checkGithubCategoryInTreeView();
        if(githubCategory == null) {
            githubCategory = new Category("Github");
            controlPaneController.getTreeViewPaneController().addNewMainCategory(githubCategory);
            return githubCategory;
        }
        else{
            return githubCategory;
        }
    }

    /**
     * Check presence category with "Github" name in main categories.
     * @return  <code>category object</code>    if category is found on the list;
     *          <code>null</code>               if object didn't find.
     */
    private Category checkGithubCategoryInTreeView() {
        for (TreeItem categoryItem : controlPaneController.getTreeViewPaneController().getListItemMainCategories()) {
            if (categoryItem.getValue().toString().equals("Github")) {
                return (Category) categoryItem.getValue();
            }
        }
        return null;
    }

    /**
     * Run function creating gist from CodeSnippet object and update it in the database.
     * @param snippets      the array with objects ready to push into repository, they are must be CodeSnippet instances.
     * @see GistCreator#createGist(CodeSnippet)
     */
    public void pushSnippets(Object[] snippets) {
        for(Object snippet:snippets){
            if(snippet instanceof CodeSnippet){
                gistCreator.createGist((CodeSnippet)snippet);
                snippetServiceDB.saveSnippetToDB((CodeSnippet)snippet);
            }
        }
    }
}
