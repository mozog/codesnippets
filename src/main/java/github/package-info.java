/**
 * This package provides class that support connection to GitHub code repository. Helps with managing uploads and downloads code snippet.
 */
package github;