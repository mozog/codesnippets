package gui.components;

import javafx.scene.control.Tab;
import javafx.scene.control.TextArea;
import models.CodeSnippet;

public class SnippetTab extends Tab {

    private TextArea textSnippetCode;
    private CodeSnippet codeSnippet;

    public SnippetTab(Object snippet){
        super(((CodeSnippet)snippet).getName());
        codeSnippet = (CodeSnippet) snippet;
        textSnippetCode = new TextArea();
        textSnippetCode.setEditable(false);
        this.setContent(textSnippetCode);
        textSnippetCode.setText(codeSnippet.getContent());
    }

    public CodeSnippet getSnippetFromTab(){
        return codeSnippet;
    }

    public TextArea getTextSnippetCode() {
        return textSnippetCode;
    }
}
