package gui.components;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class SnippetsWizardWindow implements WindowComponent {

    private static Stage stage = new Stage();
    private FXMLLoader loader;

    public SnippetsWizardWindow() {
        stage.setTitle("NewSnippet");
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL);

        loader = new FXMLLoader(getClass().getResource("/styles/NewSnippetWindow/NewSnippetWindow.fxml"));

        Parent addNewSnippetWindow;
        try {
            addNewSnippetWindow = loader.load();
            stage.setScene(new Scene(addNewSnippetWindow, 796, 787));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void showStage() {
        stage.show();
    }

    @Override
    public FXMLLoader getFXMLLoader() {
        return loader;
    }

}
