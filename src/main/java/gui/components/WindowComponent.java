package gui.components;

import javafx.fxml.FXMLLoader;

public interface WindowComponent {

    public void showStage();
    public FXMLLoader getFXMLLoader();

}
