package gui.controllers;

import gui.components.SnippetTab;
import javafx.fxml.FXML;
import services.DatabaseServices;
import utils.HibernateSessionFactory;
import utils.TagCreator;
import utils.Tags;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import models.CodeSnippet;
import models.Tag;
import org.controlsfx.control.textfield.TextFields;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CodeAndTagsPaneController {

    @FXML
    public TabPane tabPaneWithEditSnippets;
    @FXML
    public AnchorPane paneWithTabPane;
    @FXML
    public VBox vBoxSaveAndEditTags;
    @FXML
    public AnchorPane paneWithCodeAndTags;
    @FXML
    public HBox hBoxTextAreaWithSeparator;
    @FXML
    public TextField txtFieldNewTag;
    @FXML
    public ListView<Tag> listSnippetTags;

    private List<Tag> listRemovedTags = new ArrayList<>();
    private CodeSnippet codeSnippet;
    private TagCreator tagCreator = new TagCreator();
    private DatabaseServices databaseServices = new DatabaseServices();

    public TabPane getTabPaneWithEditSnippets() {
        return tabPaneWithEditSnippets;
    }

    public void init() {
        paneWithTabPane.setMinHeight(538);

        TextFields.bindAutoCompletion(txtFieldNewTag, Tags.getListTags());

        SplitPane.setResizableWithParent(paneWithCodeAndTags, true);

        AnchorPane.setRightAnchor(hBoxTextAreaWithSeparator, vBoxSaveAndEditTags.getPrefWidth() + 20.0);
        AnchorPane.setLeftAnchor(hBoxTextAreaWithSeparator, 5.0);
    }

    public void setContentToListTags(CodeSnippet codeSnippet) {
        this.codeSnippet = codeSnippet;
        listSnippetTags.getItems().clear();
        for (Tag tag : codeSnippet.getTags()) {
            listSnippetTags.getItems().add(tag);
        }
    }

    public void addNewTag(ActionEvent actionEvent) {
        if (fieldIsNotEmptyAndSnippetIsChosen()) {
            Tag tag = tagCreator.createOrFindInDBTag(txtFieldNewTag.getText());
            if (tag != null) {
                newTagIsAddedToSystem(tag);
            } else {
                tagWasInDB();
            }
        } else {
            System.out.print("An unnamed tag is not allowed or you have not selected a snippet!");
        }
        txtFieldNewTag.clear();
    }

    private void tagWasInDB() {
        if (isNewTagInSnippet()) {
            Tag tag = Tags.getTagByName(txtFieldNewTag.getText());
            tagCreator.addDependencyToTag(tag, codeSnippet);
            listSnippetTags.getItems().add(tag);
        } else {
            System.out.print("This snippet have this tag!");
        }
    }

    private void newTagIsAddedToSystem(Tag tag) {
        listSnippetTags.getItems().add(tag);
        tagCreator.addDependencyToTag(tag, codeSnippet);
        TextFields.bindAutoCompletion(txtFieldNewTag, Tags.getListTags());
    }

    private boolean fieldIsNotEmptyAndSnippetIsChosen() {
        return txtFieldNewTag.getText().length() != 0 && codeSnippet != null;
    }

    public boolean isNewTagInSnippet() {
        for (Tag tag : codeSnippet.getTags()) {
            if (tag.getName().equals(txtFieldNewTag.getText())) {
                return false;
            }
        }
        return true;
    }

    public void removeTagFromListView(ActionEvent actionEvent) {
        if (listSnippetTags.getSelectionModel().getSelectedItem() != null) {
            listRemovedTags.add(listSnippetTags.getSelectionModel().getSelectedItem());
            codeSnippet.getTags().remove(listSnippetTags.getSelectionModel().getSelectedItem());
            (listSnippetTags.getSelectionModel().getSelectedItem()).getCodeSnippets().remove(codeSnippet);
            listSnippetTags.getItems().remove(listSnippetTags.getSelectionModel().getSelectedItem());
        }
    }

    public void saveTagsToDB(ActionEvent actionEvent) {
        if (codeSnippet != null) {
            for (Object tag : listSnippetTags.getItems()) {
                databaseServices.saveOrUpdate(tag);
            }
            for (Object tag : listRemovedTags) {
                if (tag != null) {
                    databaseServices.saveOrUpdate((tag));
                }
            }
        }
    }

    public void findPhrasesInTabContent(String searchParameters) {
        Tab openedTab = tabPaneWithEditSnippets.getSelectionModel().getSelectedItem();
        if (openedTab instanceof SnippetTab) {
            if (((SnippetTab) openedTab).getTextSnippetCode().getText().toLowerCase().contains(searchParameters.toLowerCase())) {
                ((SnippetTab) openedTab).getTextSnippetCode().selectRange(((SnippetTab) openedTab).getTextSnippetCode().getText().toLowerCase().indexOf(searchParameters.toLowerCase()), searchParameters.length());
            }
        }
    }

    public void clearTagsList() {
        listSnippetTags.getItems().clear();
        codeSnippet = null;
        listRemovedTags.clear();
        listSnippetTags.refresh();
    }
}