package gui.controllers;

import alerts.AlertDialogs;
import alerts.InputDialogs;
import github.GithubConnector;
import github.GithubServices;
import gui.components.GistUploadWindow;
import gui.components.SnippetsWizardWindow;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import models.Category;
import models.CodeSnippet;
import org.controlsfx.control.PopOver;
import search.SearchParameters;
import search.SearchParser;
import search.SearchService;
import services.FileService;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

public class ControlPaneController implements Initializable {

    @FXML
    public TextField searchParametersTextField;
    @FXML
    public CheckBox showInEditorCheckBox;
    @FXML
    public Label gitLogInLabel;
    @FXML
    public Label helpLabel;
    @FXML
    public Label pushGistsLabel;
    @FXML
    public Label pullGistsLabel;

    private PopOver gitHubPopOver;
    private SearchParser searchParser = new SearchParser();
    private SearchService searchService = new SearchService();
    private FileService fileService = new FileService();
    private MainWindowController mainWindowController;
    private SnippetsWizardWindow snippetsWizardWindow = new SnippetsWizardWindow();
    private GistUploadWindow gistUploadWindow = new GistUploadWindow();
    private NewSnippetWindowController newSnippetWindowController;
    private GistManagerWindowController gistManagerWindowController;
    private TreeViewPaneController treeViewPaneController;
    private GithubServices githubServices;

    private FXMLLoader fxmlLoaderCategoryMenu = new FXMLLoader(getClass().getResource("/styles/GitHubLogWindow/GitHubLogWindow.fxml"));
    private GitHubLogWindowController gitHubLogWindowController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        newSnippetWindowController = snippetsWizardWindow.getFXMLLoader().getController();
        gistManagerWindowController = gistUploadWindow.getFXMLLoader().getController();
        githubServices = new GithubServices(this);
        gistManagerWindowController.setGithubServices(githubServices);
        try {
            Parent loginPanel = fxmlLoaderCategoryMenu.load();
            gitHubLogWindowController = fxmlLoaderCategoryMenu. getController();
            gitHubPopOver= new PopOver(loginPanel);
            gitHubPopOver.setArrowLocation(PopOver.ArrowLocation.TOP_RIGHT);
            gitHubLogWindowController.setGitHubPopOver(gitHubPopOver);
        } catch (IOException e) {
            e.printStackTrace();
        }
        setTooltips();
    }

    private void setTooltips() {
        pullGistsLabel.setTooltip(new Tooltip("Download gists from GitHub"));
        gitLogInLabel.setTooltip(new Tooltip("Connect application with GitHub account"));
        pushGistsLabel.setTooltip(new Tooltip("Push created gist to GitHub"));
        helpLabel.setTooltip(new Tooltip("How can I help you?"));
        showInEditorCheckBox.setTooltip(new Tooltip("Find first repetition in open editor"));
    }

    public void init(MainWindowController mainWindowController) {
        this.mainWindowController = mainWindowController;
        treeViewPaneController = mainWindowController.getTreeViewPaneController();
        gitHubLogWindowController.init(treeViewPaneController);
    }

    @FXML
    public void addNewSnippet() {
        newSnippetWindowController.clearControlsContent();
        newSnippetWindowController.setTreeViewPaneController(treeViewPaneController);
        newSnippetWindowController.loadCheckTreeView(treeViewPaneController.getListItemMainCategories());
        snippetsWizardWindow.showStage();
    }

    public void searchSnippets(){
        List<SearchParameters> searchParameters = searchParser.parseStringToParametersSearch(searchParametersTextField.getText());
        List<CodeSnippet> listSearchingSnippets = searchService.findSnippetsWithParameterList(searchParameters);
        mainWindowController.setResultsOnSearchPanel(listSearchingSnippets);
        if(showInEditorCheckBox.isSelected()){
            mainWindowController.findPhrasesInOpenedTab(searchParametersTextField.getText());
        }
    }


    @FXML
    public void addMainCategory(ActionEvent actionEvent) {
        InputDialogs inputDialogs = new InputDialogs();
        inputDialogs.setInputDialogInfo("New main category", "Remember name must be unique:", "Type name new category:");
        Optional<String> result = inputDialogs.getResultFromInputDialog();
        if (result.isPresent() && treeViewPaneController.isUniqueMainCategory(result.get())) {
            Category mainCategory = new Category(result.get());
            treeViewPaneController.addNewMainCategory(mainCategory);
        }
    }

    public void importSnippet(ActionEvent actionEvent) {
        FileChooser snippetChooser = new FileChooser();
        File file = snippetChooser.showOpenDialog(null);

        try {
            if(file!=null) {
                newSnippetWindowController.setContent(fileService.getStringFromFile(file));
                addNewSnippet();
            }
        } catch (IOException e) {
            AlertDialogs alertDialogs = new AlertDialogs();
            alertDialogs.setErrorDialog("Error!", "Bad file!", "The file can not be imported!");
            alertDialogs.showErrorDialog();
        }
    }

    public void exportSnippet(ActionEvent actionEvent) {
        TreeItem selectedItem = treeViewPaneController.getSelectedItem();
        if(selectedItem!=null && selectedItem.getValue() instanceof CodeSnippet){
            FileChooser saveSnippetChooser = new FileChooser();
            saveSnippetChooser.setInitialFileName(((CodeSnippet) selectedItem.getValue()).getName());
            File dest = saveSnippetChooser.showSaveDialog(null);
            if(dest!=null){
                fileService.saveContentSnippetToFile(((CodeSnippet) selectedItem.getValue()).getContent(), dest);
            }
        }
        else{
            System.out.print("choose snippet!");
        }
    }

    @FXML
    public void onEnter(ActionEvent actionEvent) {
        searchSnippets();
    }

    @FXML
    public void actionOnMouseClickedSearchLabel(MouseEvent mouseEvent) {
        searchSnippets();
    }

    public void showHelp(MouseEvent mouseEvent) {
    }

    public void logInToGitHub(MouseEvent mouseEvent) {
        gitHubLogWindowController.clearTextField();
        gitHubLogWindowController.setVisiblePanels();
        gitHubPopOver.show(gitLogInLabel);
    }

    public void pullGistsFromGitHub(MouseEvent mouseEvent) {
        githubServices.pullGists();
    }


    public void pushGistsToGitHub(MouseEvent mouseEvent) {
        if(GithubConnector.isLoggedUser()) {
            gistManagerWindowController.loadReadySnippet(treeViewPaneController.getListItemCategories());
            gistUploadWindow.showStage();
        }
    }

    public TreeViewPaneController getTreeViewPaneController(){
        return treeViewPaneController;
    }
}