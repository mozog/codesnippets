package gui.controllers;

import utils.SnippetsInfo;
import filters.Filters;
import filters.FiltersService;
import javafx.animation.RotateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.Duration;
import org.controlsfx.control.CheckComboBox;

import java.net.URL;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.ResourceBundle;

public class FiltersPaneController implements Initializable {

    public Label pinLabel;
    public RadioButton btnAscending;
    public RadioButton btnDescending;
    public ComboBox<String> sortByCombo;
    public CheckComboBox<String> languageCheckCombo;
    public CheckComboBox<String> typeCheckCombo;
    public DatePicker afterDatePicker;
    public DatePicker beforeDatePicker;
    public CheckBox favoriteCheckBox;
    public CheckComboBox<String> addedFromCheckCombo;

    private ToggleGroup toggleGroup = new ToggleGroup();
    private SearchResultsPaneController searchResultsPaneController;
    private SnippetsInfo snippetsInfo = new SnippetsInfo();
    private RotateTransition rotateTransitionPinned, rotateTransitionUnPinned;
    private Filters filters;
    private FiltersService filtersService = new FiltersService();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        pinLabel.setOnMouseClicked(event -> pinLabelMouseClicked());

        setSortComboBoxValues();

        loadCheckComboBoxes();

        rotateTransitionPinned = new RotateTransition(Duration.millis(500), pinLabel);
        rotateTransitionPinned.setByAngle(-90);

        rotateTransitionUnPinned = new RotateTransition(Duration.millis(500), pinLabel);
        rotateTransitionUnPinned.setByAngle(90);

        btnAscending.setToggleGroup(toggleGroup);
        btnDescending.setToggleGroup(toggleGroup);
    }

    private void setSortComboBoxValues() {
        sortByCombo.getItems().add("None");
        sortByCombo.getItems().add("Name");
        sortByCombo.getItems().add("Create date");
        sortByCombo.getSelectionModel().select(0);
    }

    private void loadCheckComboBoxes() {
        for(String programmingLanguage: snippetsInfo.getListProgrammingLanguage()){
            languageCheckCombo.getItems().add(programmingLanguage);
        }
        for(String type: snippetsInfo.getListTypeSnipet()){
            typeCheckCombo.getItems().add(type);
        }
        for(String addedFrom: snippetsInfo.getListLocationSnippet()){
            addedFromCheckCombo.getItems().add(addedFrom);
        }
        languageCheckCombo.getCheckModel().checkAll();
        typeCheckCombo.getCheckModel().checkAll();
        addedFromCheckCombo.getCheckModel().checkAll();
    }

    private void pinLabelMouseClicked(){
        if(searchResultsPaneController.isPinned()){
            rotateTransitionUnPinned.play();
            searchResultsPaneController.unPinnedHiddenBottomSide();
        }else{
            rotateTransitionPinned.play();
            searchResultsPaneController.pinnedHiddenBottomSide();
        }
    }

    public void init(SearchResultsPaneController searchResultsPaneController){
        this.searchResultsPaneController = searchResultsPaneController;
    }

    public void refreshResults(ActionEvent actionEvent) {
        createFiltersObject();
        searchResultsPaneController.setToListViewListSnippets(filtersService.applyFilters(searchResultsPaneController.getObservableSetToSortSnippets(), filters));
    }

    private void createFiltersObject() {
        Date addedAfter = null;
        Date addedBefore = null;
        if(afterDatePicker.getValue()!= null && beforeDatePicker.getValue()!=null) {
            LocalDate localDateAfter = afterDatePicker.getValue();
            LocalDate localDateBefore = beforeDatePicker.getValue();
            Instant instantDateAfter = Instant.from(localDateAfter.atStartOfDay(ZoneId.systemDefault()));
            Instant instantDateBefore = Instant.from(localDateBefore.atStartOfDay(ZoneId.systemDefault()));
            addedAfter = Date.from(instantDateAfter);
            addedBefore = Date.from(instantDateBefore);
        }
        String sortBy = sortByCombo.getSelectionModel().getSelectedItem().toString();
        String typeSorting = "";
        if(btnDescending.isSelected()) typeSorting = "DSC";
        else typeSorting = "ASC";
        filters = new Filters(sortBy, typeSorting, languageCheckCombo.getCheckModel().getCheckedItems(), typeCheckCombo.getCheckModel().getCheckedItems(), addedFromCheckCombo.getCheckModel().getCheckedItems(), addedAfter,addedBefore,favoriteCheckBox.isSelected());
    }
}
