package gui.controllers;

import github.GithubConnector;
import github.GithubServices;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TreeItem;
import models.Category;
import models.CodeSnippet;
import org.controlsfx.control.CheckListView;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class GistManagerWindowController {

    public Button selectAllSnippetButton;
    public CheckListView<CodeSnippet> readySnippetsList;
    public Button pushSnippetsButton;

    private GithubServices githubServices;
    private Set<CodeSnippet> readySnippetSet = new HashSet<>();
    private boolean checkedAll = false;

    public void loadReadySnippet(List<TreeItem<Object>> listItemCategories) {
        readySnippetSet.clear();
        for(TreeItem<Object> treeCategory: listItemCategories){
            for(CodeSnippet codeSnippet:((Category)treeCategory.getValue()).getCodeSnippets()){
                if(codeSnippet.getGitHubUsers().contains(GithubConnector.getGitHubUser()) && codeSnippet.getGistID().equals("local")){
                    readySnippetSet.add(codeSnippet);
                }
            }
        }
        readySnippetsList.setItems(FXCollections.observableArrayList(readySnippetSet));
    }

    public void selectAllSnippets(ActionEvent actionEvent) {
        if(checkedAll){
            readySnippetsList.getCheckModel().clearChecks();
            checkedAll=false;
        }
        else {
            readySnippetsList.getCheckModel().checkAll();
            checkedAll=true;
        }
    }

    public void pushSnippetsToGithub(ActionEvent actionEvent) {
        githubServices.pushSnippets(readySnippetsList.getCheckModel().getCheckedItems().toArray());
        ((Node) (actionEvent.getSource())).getScene().getWindow().hide();
    }

    public void setGithubServices(GithubServices githubServices) {
        this.githubServices = githubServices;
    }

    public void closeGistManagerWindow(ActionEvent actionEvent) {
        ((Node) (actionEvent.getSource())).getScene().getWindow().hide();
    }
}
