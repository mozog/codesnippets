package gui.controllers;

import github.GithubConnector;
import javafx.event.ActionEvent;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import models.GitHubUser;
import org.controlsfx.control.PopOver;
import utils.Categories;
import utils.ManagerDB;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class GitHubLogWindowController implements Initializable {

    public TextField tokenAccessTextField;
    public TextField loginTextField;
    public Button btnConnect;
    public CheckBox rememberCheckBox;
    public VBox vBoxUserLogged;
    public VBox vBoxLoginPanel;
    public ImageView avatarImageView;
    public Button disconnectButton;
    public Label userNameLabel;

    private ManagerDB managerDB = new ManagerDB();
    private PopOver gitHubPopOver;
    private TreeViewPaneController treeViewPaneController;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    public void connectUserToGitHub(ActionEvent actionEvent) {
        gitHubPopOver.hide();
        tryCreateConnection();
    }

    public void setGitHubPopOver(PopOver gitHubPopOver) {
        this.gitHubPopOver = gitHubPopOver;
    }

    private void tryCreateConnection() {
        try {
            GithubConnector.createConnectionToGitHub(loginTextField.getText(), tokenAccessTextField.getText());
            avatarImageView.setImage(GithubConnector.getAvatarImage());
            vBoxUserLogged.setVisible(true);
            vBoxLoginPanel.setVisible(false);
            GitHubUser gitHubUser;
            gitHubUser = ManagerDB.findUserWithGithubID(new Integer((new Long(GithubConnector.getGhUser().getId())).byteValue()));
            if(gitHubUser == null){
                gitHubUser = new GitHubUser(loginTextField.getText(),tokenAccessTextField.getText(),GithubConnector.getGhUser().getAvatarUrl(),rememberCheckBox.isSelected());
                gitHubUser.setGithub_ID(new Integer((new Long(GithubConnector.getGhUser().getId())).byteValue()));
            }
            userNameLabel.setText(gitHubUser.getLogin());
            gitHubUser.setRemembered(rememberCheckBox.isSelected());
            GithubConnector.setGitHubUser(gitHubUser);
            managerDB.saveGitHubUserToDB(gitHubUser);
            GithubConnector.setLoggedUser(true);
            Categories.setListCategories(managerDB.loadCategoriesFromDB());
            treeViewPaneController.loadMainTreeViewRoot();
            treeViewPaneController.refreshTreeView();
            System.out.println("Connection created!");
        } catch (IOException e) {
//            e.printStackTrace();
            System.out.println("Can not connection!");
        }
    }

    public void clearTextField() {
        loginTextField.setText("");
        tokenAccessTextField.setText("");
        rememberCheckBox.setSelected(false);
    }

    public void disconnectGitUser(ActionEvent actionEvent) {
        gitHubPopOver.hide();
        GithubConnector.closeConnection();
        Categories.setListCategories(managerDB.loadCategoriesFromDB());
        treeViewPaneController.loadMainTreeViewRoot();
        treeViewPaneController.refreshTreeView();
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        vBoxUserLogged.setVisible(false);
        vBoxLoginPanel.setVisible(true);
    }

    public void setVisiblePanels() {
        if(!GithubConnector.isLoggedUser()) {
            vBoxLoginPanel.setVisible(true);
            vBoxUserLogged.setVisible(false);
        }else{
            try {
                avatarImageView.setImage(GithubConnector.getAvatarImage());
                userNameLabel.setText(GithubConnector.getGitHubUser().getLogin());
                vBoxLoginPanel.setVisible(false);
                vBoxUserLogged.setVisible(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void init(TreeViewPaneController treeViewPaneController) {
        this.treeViewPaneController = treeViewPaneController;
    }
}