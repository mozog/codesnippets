package gui.controllers;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import models.CodeSnippet;
import search.SearchParameters;

import java.util.List;

public class MainWindowController {

    @FXML
    public AnchorPane paneWithSearchResults;
    public StackPane paneStack;

    private TabPane tabPaneWithEditSnippets;

    @FXML private CodeAndTagsPaneController codeAndTagsPaneController;

    @FXML private TreeViewPaneController treeViewPaneController;

    @FXML private ControlPaneController controlPaneController;

    @FXML private SearchResultsPaneController paneWithSearchResultsController;

    public void initialize() {
        codeAndTagsPaneController.init();
        treeViewPaneController.init(this);
        controlPaneController.init(this);
        paneWithSearchResultsController.init(this);

        tabPaneWithEditSnippets = codeAndTagsPaneController.getTabPaneWithEditSnippets();

        AnchorPane.setRightAnchor(paneStack, 28.0);
        AnchorPane.setRightAnchor(paneWithSearchResults, -(paneWithSearchResults.getPrefWidth() - 28.0));

    }

    public void setContentToSnippetTagsList(CodeSnippet codeSnippet){
        codeAndTagsPaneController.setContentToListTags(codeSnippet);
    }

    public void clearSnippetTagsList(){
        codeAndTagsPaneController.clearTagsList();
    }

    public TreeViewPaneController getTreeViewPaneController() {
        return treeViewPaneController;
    }

    public TabPane getTabPaneWithEditSnippets() {
        return tabPaneWithEditSnippets;
    }

    public void setResultsOnSearchPanel(List<CodeSnippet> codeSnippetList){
        paneWithSearchResultsController.setSearchResults(codeSnippetList);
    }

    public void findPhrasesInOpenedTab(String searchParameters) {
        codeAndTagsPaneController.findPhrasesInTabContent(searchParameters);
    }
}