package gui.controllers;

import alerts.AlertDialogs;
import github.GithubConnector;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import models.Category;
import models.CodeSnippet;
import models.Tag;
import org.controlsfx.control.CheckTreeView;
import org.controlsfx.control.ListSelectionView;
import org.controlsfx.control.ToggleSwitch;
import org.controlsfx.control.textfield.TextFields;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import services.SnippetServiceDB;
import utils.SnippetsInfo;
import utils.TagCreator;
import utils.Tags;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class NewSnippetWindowController implements Initializable {

    @FXML
    private ListSelectionView<Tag> listSelectionTags;
    @FXML
    private AnchorPane paneMarkAsFavorite;
    @FXML
    private CheckTreeView<Object> checkTreeCategories;
    @FXML
    private TextField textNameSnippet;
    @FXML
    private TextArea textAreaDescriptionSnippet;
    @FXML
    private TextField textAddedFrom;
    @FXML
    private TextField textProgrammingLanguage;
    @FXML
    private TextField textTypeSnippet;
    @FXML
    private TextField textNewTag;
    @FXML
    private TextArea textAreaContent;
    @FXML
    private FlowPane snippetWizardFlowPane;

    private ToggleSwitch toggleSwitch = new ToggleSwitch();
    private CheckBoxTreeItem<Object> root;
    private TreeViewPaneController treeViewPaneController;
    private List<CheckBoxTreeItem<Object>> checkedBox = new ArrayList<CheckBoxTreeItem<Object>>();
    private SnippetsInfo snippetsInfo = new SnippetsInfo();
    private SnippetServiceDB snippetServiceDB = new SnippetServiceDB();
    private boolean importingSnippet = false;

    private TagCreator tagCreator = new TagCreator();

    private ValidationSupport validationSupport = new ValidationSupport();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        paneMarkAsFavorite.getChildren().add(toggleSwitch);
        AnchorPane.setLeftAnchor(toggleSwitch, 100.0);

        root = new CheckBoxTreeItem<>("");
        root.setExpanded(false);
        checkTreeCategories.setRoot(root);
        checkTreeCategories.setShowRoot(false);
        checkedBox.clear();
        setValidationSupport();
        Label sourceTagsHeader = new Label("Available");
        sourceTagsHeader.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        Label targetTagsHeader = new Label("Selected");
        targetTagsHeader.setFont(Font.font("Verdana", FontWeight.BOLD, 14));
        listSelectionTags.setSourceHeader(sourceTagsHeader);
        listSelectionTags.setTargetHeader(targetTagsHeader);
    }

    public void setValidationSupport() {
        validationSupport.registerValidator(textNameSnippet, Validator.createEmptyValidator("Name is required!"));
        validationSupport.registerValidator(textAreaContent, Validator.createEmptyValidator("Content is required!"));
    }

    public void loadCheckTreeView(List<TreeItem<Object>> listMainCategories) {
        setTextFieldsCompletion();
        for (TreeItem<Object> treeItem : listMainCategories) {
            CheckBoxTreeItem<Object> checkBoxTreeItem = new CheckBoxTreeItem<>(treeItem.getValue());
            checkBoxTreeItem.selectedProperty().addListener(p -> {
                setSelectionPropertyToCheckBoxTreeItem(checkBoxTreeItem);
            });
            root.getChildren().add(checkBoxTreeItem);
            addSubCategoryToTreeItem(treeItem, checkBoxTreeItem);
        }
    }

    private void setTextFieldsCompletion() {
        TextFields.bindAutoCompletion(textProgrammingLanguage, snippetsInfo.getListProgrammingLanguage());
        TextFields.bindAutoCompletion(textAddedFrom, snippetsInfo.getListLocationSnippet());
        TextFields.bindAutoCompletion(textTypeSnippet, snippetsInfo.getListTypeSnipet());
        TextFields.bindAutoCompletion(textNewTag, Tags.getListTags());
    }

    public void setSelectionPropertyToCheckBoxTreeItem(CheckBoxTreeItem<Object> checkBoxTreeItem) {
        if (checkBoxTreeItem.isSelected()) {
            addProposedTags(checkBoxTreeItem.getValue());
            checkedBox.add(checkBoxTreeItem);
        } else {
            removeProposedTags(checkBoxTreeItem.getValue());
            checkedBox.remove(checkBoxTreeItem);
        }
    }

    public void addSubCategoryToTreeItem(TreeItem<Object> treeItem, CheckBoxTreeItem<Object> checkBoxTreeItem) {
        for (TreeItem<Object> subTreeItem : treeItem.getChildren()) {
            if (subTreeItem.getValue() instanceof Category) {
                CheckBoxTreeItem subCheckBoxTreeItem = createSubCheckAndSetSelectionProperty(subTreeItem);
                checkBoxTreeItem.getChildren().add(subCheckBoxTreeItem);
                addSubCategoryToTreeItem(subTreeItem, subCheckBoxTreeItem);
            }
        }
    }

    private CheckBoxTreeItem createSubCheckAndSetSelectionProperty(TreeItem<Object> subTreeItem) {
        CheckBoxTreeItem<Object> subCheckBoxTreeItem = new CheckBoxTreeItem<>(subTreeItem.getValue());
        subCheckBoxTreeItem.selectedProperty().addListener(p -> {
            setSelectionPropertyToCheckBoxTreeItem(subCheckBoxTreeItem);
        });
        return subCheckBoxTreeItem;
    }

    private void addProposedTags(Object category) {
        for (CodeSnippet codeSnippet : ((Category) category).getCodeSnippets()) {
            for (Tag tag : codeSnippet.getTags()) {
                if (!tagIsOnListSelection(tag)) {
                    listSelectionTags.getSourceItems().add(tag);
                }
            }
        }
    }

    private boolean tagIsOnListSelection(Tag tag) {
        return (listSelectionTags.getSourceItems().contains(tag) || listSelectionTags.getTargetItems().contains(tag));
    }

    private void removeProposedTags(Object category) {
        for (CodeSnippet codeSnippet : ((Category) category).getCodeSnippets()) {
            for (Tag tag : codeSnippet.getTags()) {
                if (listHaveTagAndIsLittleUsed(tag)) {
                    listSelectionTags.getSourceItems().remove(tag);
                }
            }
        }
    }

    private boolean listHaveTagAndIsLittleUsed(Tag tag) {
        if (listSelectionTags.getSourceItems().contains(tag)) {
            if (tag.getCodeSnippets().size() < 4) {
                return true;
            }
        }
        return false;
    }

    public void addNewSnippet(ActionEvent actionEvent) {
        CodeSnippet newCodeSnippet = null;
        try {
            newCodeSnippet = createSnippetWithManyAttribute();
            addTagsToNewSnippet(newCodeSnippet);
            addCategoriesToNewSnippet(newCodeSnippet);
            addUserToNewSnippet(newCodeSnippet);
            snippetServiceDB.saveSnippetToDB(newCodeSnippet);
            setTextFieldsCompletion();
        } catch (IOException e) {
            AlertDialogs alertDialogs = new AlertDialogs();
            alertDialogs.setErrorDialog("Error!", "Set information about snippet!", "Name and content are required!");
            alertDialogs.showErrorDialog();
            e.printStackTrace();
        }

        ((Node) (actionEvent.getSource())).getScene().getWindow().hide();
    }

    private void addUserToNewSnippet(CodeSnippet newCodeSnippet) {
        if(GithubConnector.isLoggedUser()){
            newCodeSnippet.getGitHubUsers().add(GithubConnector.getGitHubUser());
        }
    }

    private CodeSnippet createSnippetWithManyAttribute() throws IOException {
        return new CodeSnippet(textNameSnippet.getText(), textAreaContent.getText(), toggleSwitch.isSelected(), textAreaDescriptionSnippet.getText(),
                textAddedFrom.getText(), textProgrammingLanguage.getText(), textTypeSnippet.getText(), new Date());
    }

    private void addTagsToNewSnippet(CodeSnippet newCodeSnippet) {
        for (Object tag : listSelectionTags.getTargetItems()) {
            newCodeSnippet.getTags().add((Tag) tag);
            ((Tag) tag).getCodeSnippets().add(newCodeSnippet);
        }
    }

    private void addCategoriesToNewSnippet(CodeSnippet newCodeSnippet) {
        for (CheckBoxTreeItem<Object> category : checkedBox) {
            try {
                ((Category) category.getValue()).addCodeSnippetToSet(newCodeSnippet);
                newCodeSnippet.getCategories().add((Category) category.getValue());
                treeViewPaneController.addNewTreeNode(newCodeSnippet, category.getValue());
            } catch (Exception e) {
                System.out.print("Category have snippet with same name!");
                e.printStackTrace();
            }
        }
    }

    public void setTreeViewPaneController(TreeViewPaneController treeViewPaneController) {
        this.treeViewPaneController = treeViewPaneController;
    }

    public void addNewTag(ActionEvent actionEvent) {
        Tag tag;
        if (textNewTag.getText().length() != 0) {
            tag = tagCreator.createOrFindInDBTag(textNewTag.getText());
            if (tag != null) {
                listSelectionTags.getSourceItems().add(tag);
                TextFields.bindAutoCompletion(textNewTag, Tags.getListTags());
            } else {
                tag = Tags.getTagByName(textNewTag.getText());
                listSelectionTags.getSourceItems().add(tag);
            }
        }
        textNewTag.clear();
    }

    public void setContent(String contentSnippet) {
        textAreaContent.setText(contentSnippet);
        importingSnippet = true;
    }

    public void clearControlsContent() {
        listSelectionTags.getTargetItems().clear();
        listSelectionTags.getSourceItems().clear();
        checkTreeCategories.getRoot().getChildren().clear();
        if (!importingSnippet) textAreaContent.setText("");
        clearContentsInPane(snippetWizardFlowPane);
        importingSnippet = false;
    }

    private void clearContentsInPane(Pane pane) {
        for (Node node : pane.getChildren()) {
            if (node instanceof TextField) {
                ((TextField) node).setText("");
            }
            if (node instanceof ToggleSwitch) {
                ((ToggleSwitch) node).setSelected(false);
            }
            if (node instanceof TextArea) {
                ((TextArea) node).setText("");
            }
            if (node instanceof Pane) {
                clearContentsInPane((Pane) node);
            }
        }
    }
}
