package gui.controllers;

import gui.tooltips.SnippetTooltip;
import javafx.animation.TranslateTransition;
import javafx.beans.property.SimpleListProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableSet;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Side;
import javafx.scene.control.*;
import javafx.scene.input.DataFormat;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.util.Callback;
import javafx.util.Duration;
import models.Category;
import models.CodeSnippet;
import org.controlsfx.control.HiddenSidesPane;
import utils.MouseClickActions;

import java.io.IOException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

public class SearchResultsPaneController implements Initializable {


    @FXML
    public VBox vBoxSearchList;
    @FXML
    public ListView listViewSearchResults;
    @FXML
    public Button btnSearchResult;
    @FXML
    public AnchorPane paneWithSearchResults;
    @FXML
    public Label countResultsLabel;
    @FXML
    public Label arrowLabel;
    @FXML
    public Button btnRemoveResults;

    private HiddenSidesPane hiddenSidesPane = new HiddenSidesPane();
    private AnchorPane filtersPane;
    private FiltersPaneController filtersPaneController;
    private MainWindowController mainWindowController;
    private SimpleListProperty<Object> observableSetSnippets = new SimpleListProperty<>(FXCollections.observableArrayList());
    private ObservableSet<Object> setToSort = FXCollections.observableSet();
    private MouseClickActions mouseClickActions = new MouseClickActions();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        AnchorPane.setBottomAnchor(hiddenSidesPane, 5.0);
        AnchorPane.setTopAnchor(hiddenSidesPane, 3.0);
        AnchorPane.setRightAnchor(hiddenSidesPane, 4.0);

        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/styles/SearchResultsPane/FiltersPane.fxml"));

        try {
            filtersPane = fxmlLoader.load();
            filtersPaneController = fxmlLoader.getController();
            filtersPaneController.init(this);
            hiddenSidesPane.setBottom(filtersPane);
        } catch (IOException e) {
            e.printStackTrace();
        }

        hiddenSidesPane.setContent(vBoxSearchList);
        paneWithSearchResults.getChildren().add(hiddenSidesPane);

        prepareSlidePaneAnimation();

        observableSetSnippets.sizeProperty().addListener((observable, oldValue, newValue) -> { });

        countResultsLabel.textProperty().bind(observableSetSnippets.sizeProperty().asString());

        btnSearchResult.setOnDragDropped(this::actionOnDragDropped);

        btnSearchResult.setOnDragOver(this::actionOnDragOver);

        listViewSearchResults.setOnDragDropped(this::actionOnDragDropped);

        listViewSearchResults.setOnDragOver(this::actionOnDragOver);

        listViewSearchResults.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        listViewSearchResults.setCellFactory(new Callback<ListView, ListCell>() {
            @Override
            public ListCell call(ListView param) {
                ListCell<Object> cell = new ListCell<Object>() {

                    @Override
                    public void updateItem(Object item, boolean empty) {
                        super.updateItem(item, empty);
                        this.setMaxHeight(5);
                        if (item != null) {
                            setTooltip(new SnippetTooltip((CodeSnippet) item));
                            setText(item.toString());
                        }
                        else{
                            setText("");
                        }
                    }

                };
                return cell;
            }
        });

        listViewSearchResults.setOnMouseClicked(event -> {
            ObservableList listSelectedItems = listViewSearchResults.getSelectionModel().getSelectedItems();
            mouseClickActions.actionOnDoubleMouseClick(event,listSelectedItems.toArray());
        });

    }

    public boolean isPinned() {
        return hiddenSidesPane.getPinnedSide() != null;
    }

    public void unPinnedHiddenBottomSide() {
        hiddenSidesPane.setPinnedSide(null);
    }

    public void pinnedHiddenBottomSide() {
        hiddenSidesPane.setPinnedSide(Side.BOTTOM);
    }

    private void actionOnDragDropped(DragEvent event) {
        Dragboard db = event.getDragboard();
        Set<DataFormat> dataFormatSet = db.getContentTypes();
        Iterator<DataFormat> iterator;
        for (iterator = dataFormatSet.iterator(); iterator.hasNext(); ) {
            Object object = db.getContent(iterator.next());
            if (object instanceof Category) {
                addSnippetToListFromCategory((Category) object);
            } else if (object instanceof CodeSnippet) {
                    setToSort.add((CodeSnippet) object);
            }
        }
        observableSetSnippets.addAll(setToSort);
        listViewSearchResults.getItems().clear();
        listViewSearchResults.setItems(FXCollections.observableArrayList(observableSetSnippets));
        listViewSearchResults.refresh();
    }

    private void actionOnDragOver(DragEvent event) {
        event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
    }

    private void addSnippetToListFromCategory(Category category) {
        for (CodeSnippet codeSnippet : category.getCodeSnippets()) {
                setToSort.add(codeSnippet);
        }
    }

    private void prepareSlidePaneAnimation() {
        TranslateTransition openNav = new TranslateTransition(new Duration(350), paneWithSearchResults);
        TranslateTransition closeNav = new TranslateTransition(new Duration(350), paneWithSearchResults);
        openNav.setToX(0);
        btnSearchResult.setOnAction((ActionEvent evt) -> {
            if (paneWithSearchResults.getTranslateX() != 0) {
                openNav.play();
            } else {
                closeNav.setToX(-(paneWithSearchResults.getWidth() - btnSearchResult.getHeight()));
                closeNav.play();
            }
        });
    }

    public void init(MainWindowController mainWindowController) {
        this.mainWindowController = mainWindowController;
        mouseClickActions.setMainController(mainWindowController);
    }

    public void setToListViewListSnippets(ObservableList<Object> listSnippets) {
        observableSetSnippets.clear();
        observableSetSnippets.addAll(listSnippets);
        this.listViewSearchResults.setItems(FXCollections.observableArrayList(observableSetSnippets));
    }

    public ObservableSet<Object> getObservableSetToSortSnippets() {
        return setToSort;
    }

    public void removeResults(ActionEvent actionEvent) {
        List<Object> selectedItems = listViewSearchResults.getSelectionModel().getSelectedItems();
        if(selectedItems.size()!=0){
            for(Object item: selectedItems){
                observableSetSnippets.remove(item);
                setToSort.remove(item);
            }
            listViewSearchResults.setItems(FXCollections.observableArrayList(observableSetSnippets));
            listViewSearchResults.refresh();
        }
    }

    public void setSearchResults(List<CodeSnippet> searchResults) {
        observableSetSnippets.clear();
        setToSort.clear();
        for(CodeSnippet codeSnippet:searchResults)
            setToSort.add(codeSnippet);
        observableSetSnippets.addAll(setToSort);
        listViewSearchResults.setItems(FXCollections.observableArrayList(observableSetSnippets));
    }
}