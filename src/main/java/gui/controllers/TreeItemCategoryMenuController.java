package gui.controllers;

import alerts.AlertDialogs;
import alerts.InputDialogs;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import models.Category;
import org.controlsfx.control.PopOver;
import services.CategoryServiceDB;

import java.io.File;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

public class TreeItemCategoryMenuController implements Initializable {

    @FXML
    private AnchorPane paneWithDescriptionLabel;
    @FXML
    private TextField txtNameCategory;
    @FXML
    private TextField txtPathToGraphic;
    @FXML
    private TextArea txtAreaDescriptionCategory;

    private TreeViewPaneController treeViewPaneController;
    private FileChooser graphicChooser = new FileChooser();
    private PopOver popOverMenu;

    private CategoryServiceDB categoryServiceDB;
    private Category cellCategory;

    private AlertDialogs alertDialogs = new AlertDialogs();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        paneWithDescriptionLabel.setOpaqueInsets(new Insets(5, 0, 5, 10));
        graphicChooser.setTitle("Choose graphic file");
        graphicChooser.setInitialDirectory(new File(System.getProperty("user.home")));
        graphicChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Image Files", "*.jpg", "*.png"));
        categoryServiceDB = new CategoryServiceDB(this);
    }

    public void setTxtNameCategory(String categoryName) {
        txtNameCategory.setText(categoryName);
    }

    public void loadInfoToPopOverMenu(Category cellCategory) {
        this.cellCategory = cellCategory;
        setTxtNameCategory(cellCategory.getName());
        txtAreaDescriptionCategory.setText(cellCategory.getDescription());
        txtPathToGraphic.setText(cellCategory.getGraphicPath());
    }

    @FXML
    public void addCategoryToTreeView(ActionEvent actionEvent) {
        InputDialogs inputDialogs = new InputDialogs();
        inputDialogs.setInputDialogInfo("New category", "Remember name must be unique:", "Type name new category:");
        Optional<String> result = inputDialogs.getResultFromInputDialog();
        if (result.isPresent()) {
            Category subCategory = new Category(result.get());
            if(!isCategoryInChildCategories(subCategory)) {
                subCategory.setParentCategory(cellCategory);
                addNewSubCategory(subCategory);
            }
        }
    }

    public boolean isCategoryInChildCategories(Category subCategory){
        for(Category category:cellCategory.getChildCategories()){
            if(subCategory.getName().equals(category.getName())){
                return true;
            }
        }
        return false;
    }

    public void addNewSubCategory(Category subCategory) {
        cellCategory.addChildCategory(subCategory);
        categoryServiceDB.saveOrUpdateCategory(subCategory);
        treeViewPaneController.addNewTreeNode(subCategory, cellCategory);
    }

    public void choosingGraphicFile(ActionEvent actionEvent) {
        File file = graphicChooser.showOpenDialog(null);
        if (file != null) {
            cellCategory.setGraphicPath(file.getAbsolutePath());
            treeViewPaneController.refreshTreeView();
            categoryServiceDB.saveOrUpdateCategory(cellCategory);
        }
    }

    public void savePropertiesToDB(ActionEvent actionEvent) {
        setCategoryAttribute();
        categoryServiceDB.mergeCategory(cellCategory);
        popOverMenu.hide();
    }

    private void setCategoryAttribute() {
        cellCategory.setName(txtNameCategory.getText());
        cellCategory.setDescription(txtAreaDescriptionCategory.getText());
        cellCategory.setGraphicPath(txtPathToGraphic.getText());
    }

    public void cancelChangesProperties(ActionEvent actionEvent) {
        popOverMenu.hide();
    }

    public void setTreeViewPaneController(TreeViewPaneController treeViewPaneController) {
        this.treeViewPaneController = treeViewPaneController;
    }

    public void removeCategory(ActionEvent actionEvent) {
        Category parentCategory = cellCategory.getParentCategory();
        if (alertDialogs.setConfirmationDialog("Warning!", "Are you sure ?", "If you remove this category, all content will be remove!")) {
            if(parentCategory!=null){
                removeCategoryFromParent(parentCategory);
            }
            treeViewPaneController.removeCategoryFromList(cellCategory);

            categoryServiceDB.deleteCategory(cellCategory);
        } else {
            popOverMenu.hide();
        }
    }

    public void removeCategoryFromParent(Category parent) {
        parent.getChildCategories().remove(cellCategory);
        categoryServiceDB.saveOrUpdateCategory(parent);
    }

    @FXML
    public void closePopOverWindow() {
        popOverMenu.hide();
    }

    public void setPopOverMenu(PopOver popOverMenu) {
        this.popOverMenu = popOverMenu;
    }

    public TreeViewPaneController getTreeViewPaneController() {
        return treeViewPaneController;
    }
}