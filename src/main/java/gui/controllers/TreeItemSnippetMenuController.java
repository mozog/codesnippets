package gui.controllers;

import alerts.AlertDialogs;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.layout.AnchorPane;
import models.Category;
import models.CodeSnippet;
import models.Tag;
import org.controlsfx.control.PopOver;
import org.controlsfx.control.ToggleSwitch;
import services.SnippetServiceDB;
import services.TagServiceDB;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class TreeItemSnippetMenuController implements Initializable {

    @FXML
    private AnchorPane paneWithFavoriteOpt;
    @FXML
    private TextField txtFieldWithNameSnippet;
    @FXML
    private TextArea textAreaDescriptionSnippet;


    private ToggleSwitch toggleSwitch;
    private PopOver popOverMenu;
    private CodeSnippet codeSnippet;
    private TreeItem parentCategory, treeItemSnippet;
    private AlertDialogs alertDialogs = new AlertDialogs();
    private TreeViewPaneController treeViewPaneController;
    private SnippetServiceDB snippetServiceDB = new SnippetServiceDB();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        toggleSwitch =  new ToggleSwitch();
        paneWithFavoriteOpt.getChildren().add(toggleSwitch);
        AnchorPane.setLeftAnchor(toggleSwitch, 120.0);
    }

    public void saveSnippetPropertiesToDB(ActionEvent actionEvent) {
        setCodeSnippetAttribute();
        snippetServiceDB.updateSnippetToDB(codeSnippet);
        treeViewPaneController.refreshTreeView();
        popOverMenu.hide();
    }

    public void setCodeSnippetAttribute(){
        try {
            codeSnippet.setName(txtFieldWithNameSnippet.getText());
        } catch (IOException e) {
            e.printStackTrace();
        }
        codeSnippet.setDescription(textAreaDescriptionSnippet.getText());
        codeSnippet.setFavortie(toggleSwitch.isSelected());
    }

    public void cancelChangeSnippetProperties(ActionEvent actionEvent) {
        popOverMenu.hide();
    }

    public void loadInfoSnippetToPopOverMenu(CodeSnippet cellCodeSnippet, TreeItem parentCategory, TreeItem treeItemSnippet) {
        this.parentCategory = parentCategory;
        this.treeItemSnippet = treeItemSnippet;
        this.codeSnippet = cellCodeSnippet;
        txtFieldWithNameSnippet.setText(cellCodeSnippet.getName());
        textAreaDescriptionSnippet.setText(cellCodeSnippet.getDescription());
        if(cellCodeSnippet.isFavortie()){
            toggleSwitch.setSelected(true);
        }
        else{
            toggleSwitch.setSelected(false);
        }
    }

    public void removeSnippet(ActionEvent actionEvent) {
        if (alertDialogs.setConfirmationDialog("Warning!", "Are you sure ?", "If you remove this snippet, all content will be removed!")) {

            codeSnippet.getCategories().remove((Category)parentCategory.getValue());

            (((Category)parentCategory.getValue()).getCodeSnippets()).remove(codeSnippet);

            snippetServiceDB.removeOrUpdateSnippet(codeSnippet);

            parentCategory.getChildren().remove(treeItemSnippet);

            treeViewPaneController.refreshTreeView();
        } else {
            popOverMenu.hide();
        }
    }

    public void setPopOverMenu(PopOver popOverMenu) {
        this.popOverMenu = popOverMenu;
    }

    public void closePopOverWindow(ActionEvent actionEvent) {
        popOverMenu.hide();
    }

    public void setTreeViewPaneController(TreeViewPaneController treeViewPaneController) {
        this.treeViewPaneController = treeViewPaneController;
    }
}
