package gui.controllers;

import github.GithubConnector;
import gui.treecell.TreeCellFactory;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import models.Category;
import models.CodeSnippet;
import services.DatabaseServices;
import utils.Categories;
import utils.ManagerDB;
import utils.MouseClickActions;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class TreeViewPaneController implements Initializable {

    @FXML
    private TreeView<Object> treeViewSnippets;
    @FXML
    private ScrollPane scrollPaneTreeView;
    @FXML
    private AnchorPane paneTreeView;

    private List<TreeItem<Object>> listItemCategories = new ArrayList<TreeItem<Object>>();
    private List<TreeItem<Object>> listItemMainCategories = new ArrayList<>();
    private MainWindowController mainWindowController;
    private DatabaseServices databaseServices = new DatabaseServices();
    private TreeItem<Object> root = new TreeItem<>("root");
    private MouseClickActions mouseClickActions = new MouseClickActions();

    public void addActionOnTreeCell() {
        treeViewSnippets.setCellFactory(p -> new TreeCellFactory(this));
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        ManagerDB managerDB = new ManagerDB();
        managerDB.loadDateFromDB();

        treeViewSnippets.setRoot(root);
        treeViewSnippets.setShowRoot(false);

        addActionOnTreeCell();
        loadMainTreeViewRoot();

        paneTreeView.setMinWidth(250);
        paneTreeView.setMaxWidth(600);

        addActionOnDoubleMouseClicked();

        scrollPaneTreeView.setFitToWidth(true);
        scrollPaneTreeView.setFitToHeight(true);
    }

    public void loadMainTreeViewRoot() {
        listItemMainCategories.clear();
        listItemCategories.clear();
        root.getChildren().clear();
        for (Category category : Categories.getListCategories()) {
            if (category.getParentCategory() == null) {
                TreeItem<Object> treeItemCategory = new TreeItem<>(category);
                root.getChildren().add(treeItemCategory);
                listItemCategories.add(treeItemCategory);
                listItemMainCategories.add(treeItemCategory);
                addSubCategoryToCategory(category, treeItemCategory);
                addSnippetsToCategory(category, treeItemCategory);
            }
        }
    }

    public boolean isUniqueMainCategory(String nameCategory) {
        for (TreeItem<Object> object : root.getChildren()) {
            if (((Category) object.getValue()).getName().equals(nameCategory)) {
                return false;
            }
        }
        return true;
    }

    public void addNewMainCategory(Category mainCategory) {
        TreeItem<Object> treeItemCategory = new TreeItem<>(mainCategory);
        root.getChildren().add(treeItemCategory);
        listItemMainCategories.add(treeItemCategory);
        listItemCategories.add(treeItemCategory);
        databaseServices.saveOrUpdate(mainCategory);
    }

    private void addSubCategoryToCategory(Category category, TreeItem<Object> treeItem) {
        for (Category subCategory : category.getChildCategories()) {
            TreeItem<Object> itemCategory = new TreeItem<Object>(subCategory);
            treeItem.getChildren().add(itemCategory);
            addSubCategoryToCategory(subCategory, itemCategory);
            addSnippetsToCategory(subCategory, itemCategory);
            listItemCategories.add(itemCategory);
        }
    }

    private void addSnippetsToCategory(Category category, TreeItem<Object> treeItem) {
        for (CodeSnippet codeSnippet : category.getCodeSnippets()) {
            if(codeSnippet.getGitHubUsers().size()!=0) {
                if(GithubConnector.getGitHubUser()!=null && GithubConnector.isLoggedUser() && codeSnippet.containsGithubUsers(GithubConnector.getGitHubUser())) {
                    TreeItem<Object> itemSnippet = new TreeItem<>(codeSnippet);
                    treeItem.getChildren().add(itemSnippet);
                }
            }
            else{
                TreeItem<Object> itemSnippet = new TreeItem<>(codeSnippet);
                treeItem.getChildren().add(itemSnippet);
            }
        }
    }

    public void refreshTreeView() {
        treeViewSnippets.refresh();
    }

    public void addActionOnDoubleMouseClicked() {
        treeViewSnippets.setOnMouseClicked((MouseEvent event) -> {
            TreeItem<Object> selectedItem = treeViewSnippets.getSelectionModel().getSelectedItem();
            if(selectedItem!=null)
                mouseClickActions.actionOnDoubleMouseClick(event,selectedItem.getValue());
        });
    }

    public void addNewTreeNode(Object subNode, Object parentNode) {
        TreeItem<Object> treeNode = new TreeItem<Object>(subNode);
        for (TreeItem<Object> treeItem : listItemCategories) {
            if (treeItem.getValue() == parentNode) {
                treeItem.getChildren().add(treeNode);
                break;
            }
        }
        if (treeNode.getValue() instanceof Category) {
            listItemCategories.add(treeNode);
        }
        treeViewSnippets.refresh();
    }

    public void removeCategoryFromList(Category category) {
        TreeItem<Object> categoryNode = findTreeItemCategoryByObject(category);
        TreeItem<Object> parentNode;
        if (category.getParentCategory() == null) {
            listItemMainCategories.remove(findTreeItemCategoryByObject(category));
            root.getChildren().remove(categoryNode);
        } else {
            parentNode = findTreeItemCategoryByObject(category.getParentCategory());
            parentNode.getChildren().remove(categoryNode);
        }
        listItemCategories.remove(categoryNode);
    }

    public TreeItem<Object> findTreeItemCategoryByObject(Object object) {
        for (TreeItem<Object> treeItem : listItemCategories) {
            if (treeItem.getValue() == object) {
                return treeItem;
            }
        }
        return null;
    }

    public void init(MainWindowController mainWindowController) {
        this.mainWindowController = mainWindowController;
        mouseClickActions.setMainController(mainWindowController);
    }

    public List<TreeItem<Object>> getListItemMainCategories() {
        return listItemMainCategories;
    }

    public TreeItem getSelectedItem(){
        return treeViewSnippets.getSelectionModel().getSelectedItem();
    }

    public List<TreeItem<Object>> getListItemCategories(){
        return listItemCategories;
    }
}