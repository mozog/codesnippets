/**
 * Contains packages responsible for the graphic appearance of the user interface and for receiving upcoming events
 */
package gui;