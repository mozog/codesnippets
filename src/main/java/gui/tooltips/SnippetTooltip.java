package gui.tooltips;

import javafx.scene.control.Tooltip;
import models.CodeSnippet;

import java.text.SimpleDateFormat;

public class SnippetTooltip extends Tooltip {

    private String contentTooltip = "";
    private CodeSnippet codeSnippet;

    public SnippetTooltip(CodeSnippet codeSnippet) {
        this.codeSnippet = codeSnippet;
        setSnippetTooltipContent();
        setText(contentTooltip);
    }

    public void setSnippetTooltipContent(){
        String dateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(codeSnippet.getAddedDate());
        StringBuilder contentBuilder = new StringBuilder();
        contentBuilder.append("Language: " + codeSnippet.getProgrammingLanguage() + "\n");
        contentBuilder.append("Type: " + codeSnippet.getType() + "\n");
        contentBuilder.append("Location create: " + codeSnippet.getAddedFrom() + "\n");
        contentBuilder.append("Date create: " + dateString + "\n");
        contentBuilder.append("Favorite: " + codeSnippet.isFavortie() + "\n");
        contentTooltip = new String(contentBuilder);
    }

}
