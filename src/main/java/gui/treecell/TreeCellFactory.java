package gui.treecell;

import github.GithubConnector;
import utils.HibernateSessionFactory;
import gui.controllers.TreeViewPaneController;
import javafx.scene.control.*;
import javafx.scene.input.*;
import models.Category;
import models.CodeSnippet;
import org.hibernate.Session;

public class TreeCellFactory extends TreeCell<Object> {

    private static TreeItem draggedItem;
    private TreeCellLayoutCategory treeCellLayoutCategory;
    private TreeCellLayoutSnippet treeCellLayoutSnippet;
    public static DataFormat categoryFormat = new DataFormat("Category");
    public static DataFormat snippetFormat = new DataFormat("CodeSnippet");

    public TreeCellFactory(TreeViewPaneController treeViewPaneController) {
        treeCellLayoutCategory = new TreeCellLayoutCategory(treeViewPaneController);
        treeCellLayoutSnippet = new TreeCellLayoutSnippet(treeViewPaneController);
        setOnDragDetected(this::dragDetected);
        setOnDragOver(this::dragOver);
        setOnDragDropped(this::dragDropped);
    }

    private void dragDetected(MouseEvent event) {
        if (getTreeItem() != null) {
            Dragboard dragBoard = this.startDragAndDrop(TransferMode.COPY_OR_MOVE);

            draggedItem = getTreeItem();

            ClipboardContent content = new ClipboardContent();
            if (draggedItem.getValue() instanceof Category) {
                content.put(categoryFormat, (Category) getTreeItem().getValue());
            } else {
                content.put(snippetFormat, (CodeSnippet) getTreeItem().getValue());
            }
            dragBoard.setContent(content);
            dragBoard.setDragView(this.snapshot(null, null));
            event.consume();
        }
    }

    private void dragOver(DragEvent event) {
        if (!(draggedItem.getValue() instanceof Category)) {
            event.acceptTransferModes(TransferMode.MOVE);
        }
    }

    private void dragDropped(DragEvent event) {
        if (draggedItem.getValue() instanceof Category) return;
        CodeSnippet draggedSnippet = (CodeSnippet) draggedItem.getValue();
        Category categoryDropped = null;
        CodeSnippet snippetDropped = null;

        if (getTreeItem().getValue() instanceof Category) {
            if (draggedItem.getParent() == getTreeItem()) {
                return;
            }
            categoryDropped = (Category) getTreeItem().getValue();
            if (isUniqueSnippetInThisCategory(categoryDropped, draggedSnippet)) {
                System.out.print("Double snippet \n");
                return;
            }
        } else {
            if (draggedItem.getParent() == getTreeItem().getParent()) {
                return;
            }
            snippetDropped = (CodeSnippet) getTreeItem().getValue();
            if (isUniqueSnippetInThisCategory((Category) getTreeItem().getParent().getValue(), draggedSnippet)) {
                System.out.print("Double snippet\n");
                return;
            }
        }

        Category draggedItemParent = (Category) draggedItem.getParent().getValue();
        draggedItem.getParent().getChildren().remove(draggedItem);
        draggedItemParent.getCodeSnippets().remove(draggedSnippet);
        draggedSnippet.getCategories().remove(draggedItemParent);

        if (getTreeItem().getValue() instanceof Category) {
            getTreeItem().getChildren().add(draggedItem);
            draggedSnippet.getCategories().add(categoryDropped);
            categoryDropped.getCodeSnippets().add(draggedSnippet);
        } else {
            getTreeItem().getParent().getChildren().add(draggedItem);
            (draggedSnippet).getCategories().add((Category) getTreeItem().getParent().getValue());
            ((Category) getTreeItem().getParent().getValue()).getCodeSnippets().add(draggedSnippet);
        }

        if (categoryDropped != null) {
            saveDroppedChangesToDB(draggedSnippet, categoryDropped);
        } else {
            saveDroppedChangesToDB(draggedSnippet, snippetDropped);
        }

        event.setDropCompleted(true);
    }

    private boolean isUniqueSnippetInThisCategory(Category categoryDropped, CodeSnippet draggedSnippet) {
        if (categoryDropped.getCodeSnippets().contains(draggedSnippet)) {
            return true;
        }
        return false;
    }

    private void saveDroppedChangesToDB(CodeSnippet draggedSnippet, Object droppedObject) {
        Session session = HibernateSessionFactory.getSession();

        session.beginTransaction();
        if (droppedObject instanceof Category) {
            session.update((Category) droppedObject);
        } else {
            session.update((CodeSnippet) droppedObject);
        }
        session.update(draggedSnippet);

        session.getTransaction().commit();
        session.close();

    }

    @Override
    public void updateItem(Object item, boolean empty) {
        super.updateItem(item, empty);

        if (!empty && item instanceof Category) {
            treeCellLayoutCategory.setCategoryCell(amountOfUserSnippets((Category) item), item);
            setGraphic(treeCellLayoutCategory.getPaneCell());
            setText(null);
        } else if (!empty && item instanceof CodeSnippet) {
            treeCellLayoutSnippet.setSnippetCell(".  .  .", getTreeItem(), getTreeItem().getParent());
            setGraphic(treeCellLayoutSnippet.getPaneCell());
            setText(null);
        } else {
            setGraphic(null);
            setText(null);
        }

    }

    private String amountOfUserSnippets(Category itemCategory) {
        Integer amountOfSnippets = 0;
        for (CodeSnippet codeSnippet : itemCategory.getCodeSnippets()) {
            if (GithubConnector.isLoggedUser()) {
                if (codeSnippet.getGitHubUsers().contains(GithubConnector.getGitHubUser())) {
                    amountOfSnippets++;
                }
            }
            else if (codeSnippet.getGistID().equals("local")) {
                amountOfSnippets++;
            }
        }
        return amountOfSnippets.toString();
    }
}
