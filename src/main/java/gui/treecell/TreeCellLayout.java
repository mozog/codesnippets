package gui.treecell;

import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.text.Font;

public abstract class TreeCellLayout {

    ImageView graphicTreeCell = new ImageView();

    private AnchorPane paneCell;
    Button btnTreeItemMenu;
    private Label lblCellName;
    private String amountOfChildren = "0";
    String parentName = "";


    public TreeCellLayout(){
        paneCell = new AnchorPane();

        lblCellName = new Label("");
        lblCellName.setPrefWidth(150);
        graphicTreeCell.setFitHeight(20);
        graphicTreeCell.setFitWidth(20.0);

        btnTreeItemMenu = new Button(amountOfChildren);

        btnTreeItemMenu.setPrefWidth(30);
        btnTreeItemMenu.getStylesheets().add(getClass().getResource("/styles/TreeItemMenu/TreeItemCategoryMenu.css").toExternalForm());
        btnTreeItemMenu.getStyleClass().add("btnTreeItemMenuStyle");

        AnchorPane.setRightAnchor(btnTreeItemMenu,5.0);
        AnchorPane.setLeftAnchor(lblCellName,25.0);
        AnchorPane.setTopAnchor(lblCellName, 2.0);
        AnchorPane.setLeftAnchor(graphicTreeCell, 3.0);
        AnchorPane.setTopAnchor(graphicTreeCell, 2.0);

        addEventOnTreeItemButton();

        btnTreeItemMenu.setBackground(Background.EMPTY);

        paneCell.getChildren().addAll(graphicTreeCell, lblCellName,btnTreeItemMenu);
    }

    public void addEventOnTreeItemButton(){
        btnTreeItemMenu.setOnMouseMoved((event) -> {
            btnTreeItemMenu.setText(".  .  .");
            btnTreeItemMenu.setCursor(Cursor.HAND);
        });

        btnTreeItemMenu.setOnMouseExited((event)->{
            btnTreeItemMenu.setText(amountOfChildren);
        });

        btnTreeItemMenu.setOnAction((event) ->{
            showPopupOverWindow(btnTreeItemMenu);
        });
    }

    protected abstract void showPopupOverWindow(Node node);

    public void setGraphic(Image image){
        graphicTreeCell.setImage(image);
    }

    public void setButtonText(String buttonText){
        btnTreeItemMenu.setText(buttonText);
    }

    public void setLabelText(String labelText){
        lblCellName.setText(labelText);
    }

    public AnchorPane getPaneCell() {
        return paneCell;
    }

}
