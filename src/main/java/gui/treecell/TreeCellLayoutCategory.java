package gui.treecell;

import gui.controllers.TreeItemCategoryMenuController;
import gui.controllers.TreeViewPaneController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import models.Category;
import org.controlsfx.control.PopOver;

import java.io.IOException;

public class TreeCellLayoutCategory extends TreeCellLayout {

    private Parent categoryPopMenu;
    private PopOver popOver;
    private TreeItemCategoryMenuController treeItemCategoryMenuController;
    private Category cellCategory;

    public TreeCellLayoutCategory(TreeViewPaneController treeViewPaneController) {
        super();
        FXMLLoader fxmlLoaderCategoryMenu = new FXMLLoader(getClass().getResource("/styles/TreeItemMenu/TreeItemCategoryMenu.fxml"));
        try {
            categoryPopMenu = fxmlLoaderCategoryMenu.load();
            treeItemCategoryMenuController = fxmlLoaderCategoryMenu. getController();
            popOver= new PopOver(categoryPopMenu);
            popOver.setArrowLocation(PopOver.ArrowLocation.TOP_CENTER);
            treeItemCategoryMenuController.setPopOverMenu(popOver);
        } catch (IOException e) {
            e.printStackTrace();
        }
        treeItemCategoryMenuController.setTreeViewPaneController(treeViewPaneController);
    }

    @Override
    protected void showPopupOverWindow(Node node) {
        loadCategoryInfo();
        popOver.show(node);
    }

    private void loadCategoryInfo(){
        treeItemCategoryMenuController.loadInfoToPopOverMenu(cellCategory);
    }

    public void setCategoryCell(String amountOfChildren, Object treeItem) {
        cellCategory = (Category) treeItem;
        setButtonText(amountOfChildren);
        setLabelText(cellCategory.getName());
        try{
        graphicTreeCell.setImage(new Image("file:///" + cellCategory.getGraphicPath()));
        }catch(IllegalArgumentException ex){
            System.out.print("Some graphics is don't set.");
            ex.printStackTrace();
        }
        if (cellCategory.getParentCategory() != null)
            this.parentName = cellCategory.getParentCategory().getName();
        btnTreeItemMenu.setOnMouseExited((event) -> {
            btnTreeItemMenu.setText(amountOfChildren);
        });
    }
}
