package gui.treecell;

import github.GithubConnector;
import gui.controllers.TreeItemSnippetMenuController;
import gui.controllers.TreeViewPaneController;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.TreeItem;
import javafx.scene.image.ImageView;
import models.CodeSnippet;
import org.controlsfx.control.PopOver;
import org.kohsuke.github.PagedIterable;

import java.io.IOException;

public class TreeCellLayoutSnippet extends TreeCellLayout {

    private Parent snippetCell;
    private PopOver popOver;
    private TreeItemSnippetMenuController treeItemSnippetMenuController;
    private CodeSnippet cellCodeSnippet;
    private TreeItem parentCategory, treeItemSnippet;

    public TreeCellLayoutSnippet(TreeViewPaneController treeViewPaneController) {
        super();
        FXMLLoader fxmlLoaderSnippetMenu = new FXMLLoader(getClass().getResource("/styles/TreeItemMenu/TreeItemSnippetMenu.fxml"));
        try {
            snippetCell = fxmlLoaderSnippetMenu.load();
            btnTreeItemMenu.getStyleClass().add("btnTreeItemMenu");
            treeItemSnippetMenuController = fxmlLoaderSnippetMenu.getController();
            popOver= new PopOver(snippetCell);
            popOver.setArrowLocation(PopOver.ArrowLocation.TOP_CENTER);
            treeItemSnippetMenuController.setPopOverMenu(popOver);
        } catch (IOException e) {
            e.printStackTrace();
        }
        treeItemSnippetMenuController.setTreeViewPaneController(treeViewPaneController);
    }

    @Override
    protected void showPopupOverWindow(Node node) {
        loadSnippetInfo();
        popOver.show(node);
    }

    public void setSnippetCell(String amountOfChildren, TreeItem snippet, TreeItem parentCategory){
        this.parentCategory = parentCategory;
        this.treeItemSnippet = snippet;
        cellCodeSnippet = (CodeSnippet) snippet.getValue();
        setButtonText(amountOfChildren);
        setLabelText(cellCodeSnippet.getName());
        if(GithubConnector.isLoggedUser() && cellCodeSnippet.getGitHubUsers().contains(GithubConnector.getGitHubUser()) && !cellCodeSnippet.getGistID().equals("local")){
            try {
                graphicTreeCell.setImage(GithubConnector.getAvatarImage());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            graphicTreeCell.setImage(null);
        }
        btnTreeItemMenu.setOnMouseExited((event) -> {
            btnTreeItemMenu.setText(amountOfChildren);
        });
    }

    private void loadSnippetInfo(){
        treeItemSnippetMenuController.loadInfoSnippetToPopOverMenu(cellCodeSnippet, parentCategory, treeItemSnippet);
    }
}
