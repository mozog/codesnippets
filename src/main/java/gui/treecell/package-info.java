/**
 * Provides class responsible for the appearance of the cell in the directory tree.
 */
package gui.treecell;