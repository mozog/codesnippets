package models;

import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Represent category entity. Have some attribute and information about owned snippets.
 */
@Entity
@EqualsAndHashCode(exclude = {"codeSnippets", "childCategories", "id"})
@Table(name = "CATEGORY")
public class Category implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_CATEGORY", nullable = false)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne(cascade={CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinColumn(name="ID_PARENT")
    private Category parentCategory;

    @Column(name = "graphic_path")
    private String graphicPath = "";

    @OneToMany(mappedBy="parentCategory", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private Set<Category> childCategories = new HashSet<Category>();

    @ManyToMany(mappedBy="categories", cascade = {CascadeType.ALL},fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private Set<CodeSnippet> codeSnippets = new HashSet<>();

    public Category(){

    }

    public Category(String name){
        setName(name);
    }

    /**
     * Add code snippet to set. Check unique name before snippet will add.
     * @param codeSnippet       snippet who will be add to set
     * @throws Exception        throw when other snippet with the same name is present in the set
     */
    public void addCodeSnippetToSet(CodeSnippet codeSnippet) throws Exception {
        if(isUniqueName(codeSnippet)){
            codeSnippets.add(codeSnippet);
        }
        else{
            throw new Exception();
        }
    }

    /**
     * Check code snippet unique name in chosen category.
     * @param codeSnippet       checked snippet
     * @return                  <code>true</code> if snippet name is unique
     *                          <code>false</code> if snippet have the same name as other in set.
     */
    public boolean isUniqueName(CodeSnippet codeSnippet){
        for(CodeSnippet codeSnippet1: codeSnippets){
            if(codeSnippet.getName().equals(codeSnippet1.getName())) {
                return false;
            }
        }
        return true;
    }

    public void addChildCategory(Category childCategory){
        childCategories.add(childCategory);
    }

    public Set<Category> getChildCategories() {
        return childCategories;
    }

    public String getName() {
        return name;
    }

    /**
     * If name is longer than 255 characters take substring. Name in database is VARCHAR(255)
     * @param name      code snippet name
     */
    public void setName(String name) {
        if(name.length()>255){
            this.name = name.substring(0,255);
        }
        else{
            this.name = name;
        }
    }

    public Category getParentCategory() {
        return parentCategory;
    }

    public void setParentCategory(Category parentCategory) {
        this.parentCategory = parentCategory;
    }

    public Set<CodeSnippet> getCodeSnippets() {
        return codeSnippets;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGraphicPath() {
        return graphicPath;
    }

    public void setGraphicPath(String graphicPath){
        this.graphicPath = graphicPath;
    }

    public String toString(){
        return name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
}
