package models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 * Represents code snippet entity. Have information about owners (git user), tags and categories.
 */
@Entity
@EqualsAndHashCode(exclude = {"gitHubUsers", "tags", "categories", "idSnippet", "gist", "favorite"})
@Table(name = "CODE_SNIPPET")
public class CodeSnippet implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_SNIPPET", nullable = false)
    private Integer idSnippet;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "content")
    private String content;

    @Column(name = "favorite")
    private boolean favorite = false;

    @Column(name = "programming_language")
    private String programmingLanguage;

    @Column(name = "type")
    private String type;

    @Column(name = "added_from")
    private String addedFrom;

    @Column(name = "added_date")
    private Date addedDate;

    @Column(name = "gistID")
    private String gistID = "local";

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name = "SNIPPET_CATEGORY",
            joinColumns = {@JoinColumn(name = "ID_SNIPPET")},
            inverseJoinColumns = {@JoinColumn(name = "ID_CATEGORY")})
    private Set<Category> categories = new HashSet<>();

    @ManyToMany(mappedBy = "codeSnippets", cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    private Set<Tag> tags = new HashSet<>();

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE})
    @JoinTable(name = "GITUSER_SNIPPET",
            joinColumns = {@JoinColumn(name = "ID_SNIPPET")},
            inverseJoinColumns = {@JoinColumn(name = "USER_ID")})
    private Set<GitHubUser> gitHubUsers = new HashSet<>();

    public CodeSnippet() {
    }

    public CodeSnippet(String name, String content, boolean favorite, String description, String addedFrom, String programmingLanguage, String type, Date addedDate) throws IOException {
        this.setName(name);
        this.setContent(content);
        this.setFavortie(favorite);
        this.setDescription(description);
        this.setProgrammingLanguage(programmingLanguage);
        this.setAddedFrom(addedFrom);
        this.setType(type);
        this.setAddedDate(addedDate);
    }

    public Set<Category> getCategories() {
        return categories;
    }

    public void setCategories(Set categories) {
        this.categories = categories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) throws IOException {
        if (name.equals("")) throw new IOException();
        else if (name.length() > 255) {
            this.name = name.substring(0, 255);
        } else {
            this.name = name;
        }
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getContent() {
        return content;
    }

    /**
     * Before sets snippet content, checks the non-emptiness string of characters.
     * @param content           setting code snippet content
     * @throws IOException      throw where string of characters is empty
     */
    public void setContent(String content) throws IOException {
        if (content.equals("")) throw new IOException();
        else {
            this.content = content;
        }
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public boolean isFavortie() {
        return favorite;
    }

    public void setFavortie(boolean favortie) {
        this.favorite = favortie;
    }

    public String getAddedFrom() {
        return addedFrom;
    }

    public void setAddedFrom(String addedFrom) {
        if (addedFrom.length() > 255) {
            this.addedFrom = addedFrom.substring(0, 255);
        } else {
            this.addedFrom = addedFrom;
        }
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        if (type.length() > 255) {
            this.type = type.substring(0, 255);
        } else {
            this.type = type;
        }
    }

    public String getProgrammingLanguage() {
        return programmingLanguage;
    }

    public void setProgrammingLanguage(String programmingLanguage) {
        if (programmingLanguage.length() > 255) {
            this.programmingLanguage = programmingLanguage.substring(0, 255);
        } else {
            this.programmingLanguage = programmingLanguage;
        }
    }

    public String getGistID() {
        return gistID;
    }

    public void setGistID(String gistID) {
        this.gistID = gistID;
    }

    public Set<GitHubUser> getGitHubUsers() {
        return gitHubUsers;
    }

    /**
     * Check present github user in snippet users set.
     * @param gitHubUser        searched github user
     * @return                  <code>true</code> if snippet contains user
     *                          <code>false</code> if snippet don't contain user
     */
    public boolean containsGithubUsers(GitHubUser gitHubUser) {
        for(GitHubUser gitHubUser1:gitHubUsers){
            if(gitHubUser.equals(gitHubUser1)){
                return true;
            }
        }
        return false;
    }
}
