package models;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Represent Github user model. Besides basic attribute storage information about owned snippet.
 */
@Entity
@Table(name = "GITHUB_USER")
public class GitHubUser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "USER_ID", nullable = false)
    private Integer user_ID;

    @Column(name = "GITHUB_ID")
    private Integer github_ID;

    @Column(name = "LOGIN")
    private String login;

    @Column(name = "oauth_tokenaccess")
    private String oauthTokenAccess;

    @Column(name = "urlImage")
    private String urlImage;

    @Column(name = "remembered")
    private boolean remembered;

    @ManyToMany(cascade = CascadeType.MERGE, mappedBy = "gitHubUsers", fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    private Set<CodeSnippet> userSnippets = new HashSet<>();

    public GitHubUser() {

    }

    public GitHubUser(String login, String token, String urlImage, boolean remembered) {
        this.login = login;
        this.oauthTokenAccess = token;
        this.urlImage = urlImage;
        this.remembered = remembered;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null || getClass() != object.getClass()) {
            return false;
        }
        return user_ID.equals(((GitHubUser) object).getUser_ID());
    }

    @Override
    public int hashCode() {
        return user_ID;
    }

    public Set<CodeSnippet> getUserSnippets() {
        return userSnippets;
    }

    public String getLogin() {
        return login;
    }

    public String getOauthTokenAccess() {
        return oauthTokenAccess;
    }

    public boolean isRemembered() {
        return remembered;
    }

    public void setRemembered(boolean remembered) {
        this.remembered = remembered;
    }

    public Integer getGithub_ID() {
        return github_ID;
    }

    public void setGithub_ID(Integer github_ID) {
        this.github_ID = github_ID;
    }

    public Integer getUser_ID() {
        return user_ID;
    }

    public void removeSnippetFromUser(CodeSnippet codeSnippet) {
        userSnippets.remove(codeSnippet);
    }
}
