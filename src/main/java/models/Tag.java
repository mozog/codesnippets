package models;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents model tag who describe code snippet. Have information about assigned snippet.
 */
@Entity
@EqualsAndHashCode(exclude = {"codeSnippets","idTag"} )
@Table(name = "TAG")
public class Tag implements Serializable{

    @Id
    @Getter
    @Setter
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID_TAG", nullable = false)
    private Integer idTag;

    @Column(name = "name")
    private String name;

    @Column(name = "added_date")
    private Date addedDate;

    @ManyToMany(cascade = {CascadeType.ALL}, fetch = FetchType.EAGER)
    @JoinTable(name="SNIPPET_TAG",
            joinColumns={@JoinColumn(name="ID_TAG")},
            inverseJoinColumns={@JoinColumn(name="ID_SNIPPET")})
    private Set<CodeSnippet> codeSnippets = new HashSet<>();

    public Tag() {
    }

    public Tag(String name, Date addedDate){
        this.addedDate = addedDate;
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if(name.length()>255){
            this.name = name.substring(0,255);
        }
        else{
            this.name = name;
        }
    }

    @Override
    public String toString(){
        return name;
    }

    public Set<CodeSnippet> getCodeSnippets() {
        return codeSnippets;
    }

}