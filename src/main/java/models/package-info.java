/**
 * Provides models class which are mapped to database schema.
 */
package models;