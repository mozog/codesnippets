package search;

import org.hibernate.Session;
import org.hibernate.query.Query;

/**
 * QueryCreator is responsible for creating queries with search parameters to database.
 */
public class QueryCreator {

    private static String queryCategoryParam = "select distinct cs from CodeSnippet cs join cs.categories c where c.name like (:category)";
    private static String queryNameSnippetParam = "select distinct cs from CodeSnippet cs where cs.name like (:name)";
    private static String queryTagParam = "select distinct cs from CodeSnippet cs join cs.tags t where t.name like (:tag)";
    private static String querySnippetContentParam = "select distinct cs from CodeSnippet cs where cs.content like (:content)";
    private Query searchQueryByCategories;
    private Query searchQueryByTags;
    private Query searchQueryByName;
    private Query searchQueryByContent;

    /**
     * Generate Query object from declared string.
     * @param session       actual hibernate session who can create queries
     * @see Query
     * @see Session#createQuery(String)
     */
    public QueryCreator(Session session){
        searchQueryByCategories = session.createQuery(queryCategoryParam);
        searchQueryByName = session.createQuery(queryNameSnippetParam);
        searchQueryByTags = session.createQuery(queryTagParam);
        searchQueryByContent = session.createQuery(querySnippetContentParam);
    }

    /**
     * One of sets parameters function. Set category parameter in Query object.
     * @param parameter     parameter with category attribute
     * @return              query object containing SQL query with input parameter
     */
    public Query setParameterInQueryCategories(String parameter){
        searchQueryByCategories.setParameter("category", parameter);
        return searchQueryByCategories;
    }

    public Query setParameterInQueryNames(String parameter){
        searchQueryByName.setParameter("name", parameter);
        return searchQueryByName;
    }

    public Query setParameterInQueryTags(String parameter){
        searchQueryByTags.setParameter("tag", parameter);
        return searchQueryByTags;
    }

    public Query setParameterInQueryContents(String parameter){
        searchQueryByContent.setParameter("content", parameter);
        return searchQueryByContent;
    }

}
