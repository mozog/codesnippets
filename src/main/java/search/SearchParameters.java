package search;

import models.Category;
import utils.Categories;

import java.util.ArrayList;
import java.util.List;

/**
 * Class responsible for storage search parameters.
 */
public class SearchParameters {

    private List<String> categories = new ArrayList<>();
    private List<String> tags = new ArrayList<>();
    private List<String> snippetNames = new ArrayList<>();
    private List<String> contents = new ArrayList<>();

    /**
     * Add category with two % sign to list. It will be used to creating queries to database.If full name of category is present in actual set,
     * additionally add subcategory.
     * @param categoryName      one of the search attribute
     */
    public void addCategory(String categoryName) {
        Category category = Categories.getCategoryByName(categoryName);
        if(category!=null){
            addChildCategoryToCategories(category);
        }
        categories.add("%" + categoryName + "%");
    }

    /**
     * Recursive function who adds subcategory from input category as search parameters.
     * @param category      name of category with subcategory
     */
    public void addChildCategoryToCategories(Category category){
        for(Category childCategory:category.getChildCategories()){
            categories.add("%" + childCategory.getName() + "%");
            addChildCategoryToCategories(childCategory);
        }
    }

    /**
     * Add phrases to search parameters, who will be search in snippet content.
     * @param content       part of content searching code snippet
     */
    public void addContents(String content){
        contents.add("%" + content + "%");
    }

    public void addTag(String tag) {
        tags.add("%" + tag + "%");
    }

    public void addSnippetName(String snippetName) {
        snippetNames.add("%" + snippetName + "%");
    }

    public List<String> getCategories() {
        return categories;
    }

    public List<String> getTags() {
        return tags;
    }

    public List<String> getSnippetNames() {
        return snippetNames;
    }

    public List<String> getContents(){
        return contents;
    }

}
