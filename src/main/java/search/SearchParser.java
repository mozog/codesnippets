package search;

import java.util.ArrayList;
import java.util.List;

/**
 * Class responsible for getting search parameters from a string of characters entered by user.
 */
public class SearchParser {

    private List<SearchParameters> searchParametersList = new ArrayList<>();

    /**
     * Divides string by AND operator and create SearchParameters objects to each of substring to storage his attributes.
     * @param stringParameter       the string of characters with special signs, regular expression
     * @return                      list with objects which storage parameters each of substring
     * @see SearchParser#createParametersByOR(String)
     * @see SearchParameters
     */
    public List<SearchParameters> parseStringToParametersSearch(String stringParameter){
        String[] parametersDividedByAND = stringParameter.split("&&");
        searchParametersList.clear();
        for(String parameterDividedByAND: parametersDividedByAND){
            searchParametersList.add(createParametersByOR(parameterDividedByAND));
        }
        return searchParametersList;
    }

    /**
     * Divides string of characters by OR operator and create object with parameters which were contained in substring.
     * @param parametersInString        regular expression with only OR operator
     * @return                          object with assigned search parameters
     * @see SearchParameters
     */
    public SearchParameters createParametersByOR(String parametersInString){
        String[] parameters = parametersInString.split("[|]{2}");
        SearchParameters searchParameters = new SearchParameters();
        for(String parameter: parameters){
            parameter = parameter.trim();
            if(parameter.length()>0) {
                if (parameter.charAt(0) == '#') {
                    searchParameters.addTag(parameter.substring(1));
                }
                else if (parameter.charAt(0) == '@') {
                    searchParameters.addCategory(parameter.substring(1));
                }
                else if(parameter.charAt(0) == '$'){
                    searchParameters.addContents(parameter.substring(1));
                }
                else {
                    searchParameters.addSnippetName(parameter);
                }
            }
        }
        return searchParameters;
    }

}
