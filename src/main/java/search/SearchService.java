package search;

import github.GithubConnector;
import models.CodeSnippet;
import org.hibernate.Session;
import utils.HibernateSessionFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Class responsible for generating search results. Gets queries results and creates code snippet list.
 */
public class SearchService {

    private QueryCreator queryCreator;

    private Set<CodeSnippet> setSearchResults = new HashSet<>();

    /**
     * Main function who finding and filtering searched code snippet.
     * @param parametersList        list with SearchParameters object
     * @return                      the list with matching code snippets to parameters
     */
    public List<CodeSnippet> findSnippetsWithParameterList(List<SearchParameters> parametersList){
        List<List<CodeSnippet>> foundedCodeSnippets = new ArrayList<>();
        List<CodeSnippet> resultsList = new ArrayList<>();
        for(SearchParameters searchParameter: parametersList){
            foundedCodeSnippets.add(getSnippetsWithParameters(searchParameter));
        }
        if(foundedCodeSnippets.size()>0) {
            for (CodeSnippet foundedSnippet : foundedCodeSnippets.get(0)) {
                int count = 1;
                for (int i = 1; i < foundedCodeSnippets.size(); i++) {
                    if (foundedCodeSnippets.get(i).contains(foundedSnippet)) {
                        count++;
                    }
                }
                if (count == foundedCodeSnippets.size()) {
                    resultsList.add(foundedSnippet);
                }
            }
        }
        return filterResultsByUsers(resultsList);
    }

    /**
     *
     * @param searchParameters      object with restrictions to code snippets
     * @return                      list snippet with searched parameters
     */
    public List<CodeSnippet> getSnippetsWithParameters(SearchParameters searchParameters) {
        setSearchResults.clear();

        Session session = HibernateSessionFactory.getSession();
        session.beginTransaction();

        queryCreator = new QueryCreator(session);

        searchByName(searchParameters.getSnippetNames(), session);
        searchByCategories(searchParameters.getCategories(), session);
        searchByTags(searchParameters.getTags(), session);
        searchByContent(searchParameters.getContents(), session);

        session.getTransaction().commit();
        session.close();

        return new ArrayList<>(setSearchResults);
    }

    /**
     * Check list with snippets. If snippet doesn't belong to the user it is not added to results list.
     * @param setSearchResults      results list without filter by user
     * @return                      snippets belongs to logged user
     */
    private List<CodeSnippet> filterResultsByUsers(List<CodeSnippet> setSearchResults) {
        List<CodeSnippet> filtersByUsersSnippets = new ArrayList<>();
        for(CodeSnippet codeSnippet:setSearchResults){
            if(GithubConnector.isLoggedUser()){
                if(codeSnippet.getGitHubUsers().contains(GithubConnector.getGitHubUser()) || codeSnippet.getGistID().equals("local")){
                    filtersByUsersSnippets.add(codeSnippet);
                }
            }else{
                if(codeSnippet.getGistID().equals("local")){
                    filtersByUsersSnippets.add(codeSnippet);
                }
            }
        }
        return filtersByUsersSnippets;
    }

    private void searchByContent(List<String> contents, Session session) {
        for(String content: contents){
            setSearchResults.addAll(queryCreator.setParameterInQueryContents(content).list());
        }
    }

    private void searchByName(List<String> snippetNames, Session session) {
        for (String name : snippetNames) {
            setSearchResults.addAll(queryCreator.setParameterInQueryNames(name).list());
        }
    }

    private void searchByCategories(List<String> categories, Session session) {
        for (String category : categories) {
            setSearchResults.addAll(queryCreator.setParameterInQueryCategories(category).list());
        }
    }

    private void searchByTags(List<String> tags, Session session) {
        for(String tag: tags){
            setSearchResults.addAll(queryCreator.setParameterInQueryTags(tag).list());
        }
    }
}