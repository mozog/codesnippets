/**
 * Provide classes that support searching code snippet. They are use with search bar located in toolbar.
 */
package search;