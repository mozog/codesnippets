package services;

import gui.controllers.TreeItemCategoryMenuController;
import models.Category;
import models.CodeSnippet;
import models.GitHubUser;
import models.Tag;
import utils.ManagerDB;

import java.util.HashSet;
import java.util.Set;

public class CategoryServiceDB {

    private DatabaseServices databaseServices = new DatabaseServices();
    private SnippetServiceDB snippetServiceDB = new SnippetServiceDB();
    private TreeItemCategoryMenuController treeItemCategoryMenuController;

    public CategoryServiceDB(TreeItemCategoryMenuController treeItemCategoryMenuController){
        this.treeItemCategoryMenuController = treeItemCategoryMenuController;
    }

    public void saveOrUpdateCategory(Category category){
        databaseServices.saveOrUpdate(category);
    }

    public void mergeCategory(Category cellCategory) {
        databaseServices.merge(cellCategory);
    }

    public void deleteCategory(Category category){
        removeAndUpdateSnippets(category);
        removeSubcategory(category);
        category.setParentCategory(null);
        category.getChildCategories().clear();

        databaseServices.delete(category);
    }
    private void removeAndUpdateSnippets(Category category) {
        Set<CodeSnippet> categorySnippets = category.getCodeSnippets();
        for (CodeSnippet snippet : categorySnippets) {
            snippet.getCategories().remove(category);
            snippetServiceDB.removeOrUpdateSnippet(snippet);
        }
        categorySnippets.clear();
        category.getCodeSnippets().clear();
    }

    private void removeSubcategory(Category category) {
        for (Category subCategory : category.getChildCategories()) {
            subCategory.setParentCategory(null);
            treeItemCategoryMenuController.getTreeViewPaneController().removeCategoryFromList(subCategory);
            this.deleteCategory(subCategory);
        }
    }

}
