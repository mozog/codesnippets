package services;

import models.Category;
import models.CodeSnippet;
import models.GitHubUser;
import models.Tag;
import org.hibernate.Session;
import utils.HibernateSessionFactory;

public class DatabaseServices {

    public void saveOrUpdate(Object object){
        Session session = HibernateSessionFactory.getSession();
        session.beginTransaction();

        session.saveOrUpdate(object);

        session.getTransaction().commit();
        session.close();
    }

    public void update(Object object){
        Session session = HibernateSessionFactory.getSession();
        session.beginTransaction();


        session.update(object);

        session.getTransaction().commit();
        session.close();
    }

    public void delete(Object object){
        Session session = HibernateSessionFactory.getSession();
        session.beginTransaction();

        session.delete(object);

        session.getTransaction().commit();
        session.close();
    }

    public void merge(Category object){
        Session session = HibernateSessionFactory.getSession();
        session.beginTransaction();

        session.merge(object);

        session.getTransaction().commit();
        session.close();
    }

}
