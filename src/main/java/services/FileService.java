package services;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.MalformedInputException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileService {

    public FileService(){
    }

    public String getStringFromFile(File file) throws IOException, MalformedInputException {
        if(file == null) throw new IOException();
        StringBuilder stringBuilder = new StringBuilder();
        List<String> lines = Files.readAllLines(Paths.get(file.getAbsolutePath()));
        for(String line: lines){
            stringBuilder.append(line + System.getProperty("line.separator"));
        }
        return new String(stringBuilder);
    }

    public void saveContentSnippetToFile(String content, File dest) {
        try (PrintWriter out = new PrintWriter(dest)) {
            out.println(content);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
