package services;

import models.CodeSnippet;
import models.GitHubUser;
import models.Tag;
import utils.SnippetsInfo;

import java.util.List;

public class SnippetServiceDB {

    private static List<CodeSnippet> codeSnippetList;
    private SnippetsInfo snippetsInfo = new SnippetsInfo();
    private DatabaseServices databaseServices = new DatabaseServices();
    private TagServiceDB tagServiceDB = new TagServiceDB();

    public void saveSnippetToDB(CodeSnippet newCodeSnippet) {
        databaseServices.saveOrUpdate(newCodeSnippet);
    }

    public void updateSnippetToDB(CodeSnippet codeSnippet){
        databaseServices.saveOrUpdate(codeSnippet);
    }


    public void deleteSnippetFromDB(CodeSnippet codeSnippet){
        databaseServices.delete(codeSnippet);
    }

    public void addNewSnippetAndLoadInfo(CodeSnippet newCodeSnippet) {
        codeSnippetList.add(newCodeSnippet);
        snippetsInfo.addLocationToList(newCodeSnippet.getAddedFrom());
        snippetsInfo.addProgrammingLanguageToList(newCodeSnippet.getProgrammingLanguage());
        snippetsInfo.addTypeToList(newCodeSnippet.getType());
    }

    public void removeOrUpdateSnippet(CodeSnippet codeSnippet) {
        if(codeSnippet.getCategories().size()==0){
            removeUsersFromSnippets(codeSnippet);
            removeTagsFromSnippet(codeSnippet);
            deleteSnippetFromDB(codeSnippet);
        }
        else{
            updateSnippetToDB(codeSnippet);
        }
    }

    private void removeUsersFromSnippets(CodeSnippet snippet) {
        for(GitHubUser gitHubUser:snippet.getGitHubUsers()){
            gitHubUser.removeSnippetFromUser(snippet);
        }
        snippet.getGitHubUsers().clear();
    }

    private void removeTagsFromSnippet(CodeSnippet codeSnippet) {
        for(Tag tag : codeSnippet.getTags()){
            tag.getCodeSnippets().remove(codeSnippet);
        }
        tagServiceDB.updateTags(codeSnippet.getTags());
        codeSnippet.getTags().clear();
    }

    public static void setCodeSnippetList(List<CodeSnippet> codeSnippetList) {
        SnippetServiceDB.codeSnippetList = codeSnippetList;
    }

    public static List<CodeSnippet> getCodeSnippetList() {
        return codeSnippetList;
    }

}
