package services;

import models.Tag;
import services.DatabaseServices;
import utils.Tags;

import java.util.Set;

public class TagServiceDB {

    private Tags tags = new Tags();
    private DatabaseServices databaseServices = new DatabaseServices();


    public void updateTags(Set<Tag> listTags) {
        for (Tag tag : listTags) {
            databaseServices.update(tag);
        }
    }

    public Tags getTags() {
        return tags;
    }

    public void setTags(Tags tags) {
        this.tags = tags;
    }
}
