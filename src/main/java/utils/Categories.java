package utils;

import models.Category;

import java.util.ArrayList;
import java.util.List;

public class Categories {

    private static List<Category> listCategories = new ArrayList<>();

    public static Category getCategoryByName(String nameCategory) {
        for (Category category : listCategories) {
            if (category.getName().equals(nameCategory)) {
                return category;
            }
        }
        return null;
    }

    public static List<Category> getListCategories() {
        return listCategories;
    }

    public static void setListCategories(List<Category> listCategories) {
        Categories.listCategories = listCategories;
    }
}
