package utils;

import github.GithubConnector;
import gui.controllers.ControlPaneController;
import models.Category;
import models.CodeSnippet;
import org.kohsuke.github.GHGist;
import org.kohsuke.github.GHGistFile;
import services.SnippetServiceDB;

import java.io.IOException;
import java.util.Map;

import static com.google.common.io.Files.getNameWithoutExtension;

public class CodeSnippetCreator {

    private ManagerDB managerDB = new ManagerDB();
    private ControlPaneController controlPaneController;
    private SnippetServiceDB snippetServiceDB = new SnippetServiceDB();

    public CodeSnippetCreator(ControlPaneController controlPaneController){
        this.controlPaneController = controlPaneController;
    }

    public CodeSnippet createOrFindInDBSnippet(GHGist ghGist, Map<String, GHGistFile> gistFiles, Category categoryForCodeSnippet) throws Exception {
        CodeSnippet newCodeSnippet;
        newCodeSnippet = findSnippetByGistID(ghGist.getHtmlUrl().getPath().substring(1,ghGist.getHtmlUrl().getPath().length()));
        if(newCodeSnippet == null) {
            newCodeSnippet = new CodeSnippet();
            loadInfoFromGists(newCodeSnippet, ghGist);
            newCodeSnippet.setName(getNameWithoutExtension(gistFiles.keySet().iterator().next()));
            newCodeSnippet.setContent(createCodeSnippetContent(gistFiles));

            addCategoryAndUserToSnippet(newCodeSnippet, categoryForCodeSnippet);
            snippetServiceDB.addNewSnippetAndLoadInfo(newCodeSnippet);

            controlPaneController.getTreeViewPaneController().addNewTreeNode(newCodeSnippet, categoryForCodeSnippet);

        }else {
            loadInfoFromGists(newCodeSnippet, ghGist);
            newCodeSnippet.setName(getNameWithoutExtension(gistFiles.keySet().iterator().next()));
            newCodeSnippet.setContent(createCodeSnippetContent(gistFiles));
        }
        return newCodeSnippet;
    }

    private void addCategoryAndUserToSnippet(CodeSnippet newCodeSnippet, Category categoryForCodeSnippet) throws Exception {
        categoryForCodeSnippet.addCodeSnippetToSet(newCodeSnippet);
        newCodeSnippet.getCategories().add(categoryForCodeSnippet);
        GithubConnector.getGitHubUser().getUserSnippets().add(newCodeSnippet);
        newCodeSnippet.getGitHubUsers().add(GithubConnector.getGitHubUser());
    }

    private CodeSnippet findSnippetByGistID(String gistID) {
        return managerDB.findSnippetByGitID(gistID);
    }

    private String createCodeSnippetContent(Map<String, GHGistFile> gistFiles) {
        StringBuilder snippetContent = new StringBuilder();
        for (String key : gistFiles.keySet()) {
            loadInfoFromGistsFiles(snippetContent,gistFiles.get(key));
        }
        return snippetContent.toString();
    }

    private void loadInfoFromGistsFiles(StringBuilder snippetContent, GHGistFile ghGistFile) {
        snippetContent.append(ghGistFile.getFileName()).append("\n");
        snippetContent.append(HttpReader.readTextFromUrl(ghGistFile.getRawUrl())).append("\n\n");
    }

    private void loadInfoFromGists(CodeSnippet newCodeSnippet, GHGist ghGist) throws IOException {
        newCodeSnippet.setGistID(String.valueOf(ghGist.getHtmlUrl().getPath()).substring(1,ghGist.getHtmlUrl().getPath().length()));
        newCodeSnippet.setAddedDate(ghGist.getCreatedAt());
        newCodeSnippet.setDescription(ghGist.getDescription());
    }
}
