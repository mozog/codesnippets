package utils;

import github.GithubConnector;
import models.Category;
import models.CodeSnippet;
import models.GitHubUser;
import models.Tag;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import services.DatabaseServices;
import services.SnippetServiceDB;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ManagerDB {

    private static Categories categories = new Categories();
    private SnippetsInfo snippetsInfo = new SnippetsInfo();
    private DatabaseServices databaseServices = new DatabaseServices();
    private static List<GitHubUser> gitHubUsers = new ArrayList<GitHubUser>();

    public static List<GitHubUser> getGitHubUsers() {
        return gitHubUsers;
    }

    public void loadDateFromDB() {
        gitHubUsers = loadUsersFromDB();
        tryLoginUser(findRememberedUser());
        categories.setListCategories(loadCategoriesFromDB());
        Tags.setListTags(loadTagsFromDB());
        SnippetServiceDB.setCodeSnippetList(loadSnippetsFromDB());
        loadSnippetsInfo();
    }

    private List<GitHubUser> loadUsersFromDB() {
        Session session = HibernateSessionFactory.getSession();
        session.beginTransaction();
        List listUsers = session.createQuery("from GitHubUser ").list();
        session.getTransaction().commit();
        session.close();
        return listUsers;
    }

    private void tryLoginUser(GitHubUser rememberedUser) {
        if(rememberedUser!=null) {
            try {
                GithubConnector.createConnectionToGitHub(rememberedUser.getLogin(), rememberedUser.getOauthTokenAccess());
                GithubConnector.setGitHubUser(rememberedUser);
                GithubConnector.setLoggedUser(true);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private GitHubUser findRememberedUser() {
        for(GitHubUser gitHubUser:gitHubUsers){
            if(gitHubUser.isRemembered()){
                return gitHubUser;
            }
        }
        return null;
    }

    private List<Tag> loadTagsFromDB() {
        Session session = HibernateSessionFactory.getSession();
        session.beginTransaction();
        List listTags = session.createQuery("from Tag").list();
        session.getTransaction().commit();
        session.close();
        return listTags;
    }

    private List<CodeSnippet> loadSnippetsFromDB() {
        Session session = HibernateSessionFactory.getSession();
        session.beginTransaction();
        List listSnippets = session.createQuery("from CodeSnippet ").list();
        session.getTransaction().commit();
        session.close();
        return listSnippets;
    }

    public List<Category> loadCategoriesFromDB() {
        Session session = HibernateSessionFactory.getSession();
        session.beginTransaction();
        List listCategories = session.createQuery("from Category").list();
        session.getTransaction().commit();
        session.close();
        return listCategories;
    }

    private void loadSnippetsInfo() {
        Session session = HibernateSessionFactory.getSession();
        session.beginTransaction();
        List listProgrammingLanguage = session.createQuery("select distinct c.programmingLanguage from CodeSnippet c").list();
        List listTypeSnippets = session.createQuery("select distinct c.type from CodeSnippet c").list();
        List listLocationSnippets = session.createQuery("select distinct c.addedFrom from CodeSnippet as c").list();
        session.getTransaction().commit();
        session.close();

        listProgrammingLanguage = replaceNullObjectInList(listProgrammingLanguage);
        listTypeSnippets = replaceNullObjectInList(listTypeSnippets);
        listLocationSnippets = replaceNullObjectInList(listLocationSnippets);

        snippetsInfo.setListProgrammingLanguage(listProgrammingLanguage);
        snippetsInfo.setListTypeSnippet(listTypeSnippets);
        snippetsInfo.setListLocationSnippet(listLocationSnippets);
    }

    public List replaceNullObjectInList(List listWithNull){
        if(listWithNull.contains(null)) {
            listWithNull.remove(null);
            listWithNull.add("none");
        }
        return listWithNull;
    }

    public void saveGitHubUserToDB(GitHubUser gitHubUser) {
        databaseServices.saveOrUpdate(gitHubUser);
    }

    public static GitHubUser findUserWithGithubID(Integer integer) {
        for(GitHubUser gitHubUser: gitHubUsers){
            if(gitHubUser.getGithub_ID().equals(integer)){
                return gitHubUser;
            }
        }
        return null;
    }

    public CodeSnippet findSnippetByGitID(String gistID) {
        CodeSnippet foundedSnippet;
        Session session = HibernateSessionFactory.getSession();
        session.beginTransaction();
        Criteria criteria = session.createCriteria(CodeSnippet.class);
        foundedSnippet = (CodeSnippet) criteria.add(Restrictions.eq("gistID", gistID))
                .uniqueResult();
        session.getTransaction().commit();
        session.close();
        return foundedSnippet;
    }
}
