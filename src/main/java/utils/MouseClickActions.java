package utils;

import gui.components.SnippetTab;
import gui.controllers.MainWindowController;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.input.MouseEvent;
import models.CodeSnippet;

public class MouseClickActions {

    private MainWindowController mainWindowController;

    public void actionOnDoubleMouseClick(MouseEvent event, Object... selectedItems) {
        if (event.getClickCount() == 2 && selectedItems.length != 0) {
            for (Object object : selectedItems) {
                if (object instanceof CodeSnippet) {
                    if (isTabOpening(object.toString())) {
                        mainWindowController.getTabPaneWithEditSnippets().getSelectionModel().select(getTabByName(object.toString()));
                    } else {
                        createNewTab(object);
                    }
                    mainWindowController.setContentToSnippetTagsList((CodeSnippet) object);
                }
            }
        }
    }

    private void createNewTab(Object object) {
        SnippetTab tab = new SnippetTab(object);
        tab.setOnClosed(event1 -> {
            mainWindowController.clearSnippetTagsList();
            if (mainWindowController.getTabPaneWithEditSnippets().getTabs().size() != 0) {
                SingleSelectionModel<Tab> selectionModel = mainWindowController.getTabPaneWithEditSnippets().getSelectionModel();
                selectionModel.select(0);
            }
        });
        tab.setOnSelectionChanged(event2 -> {
            if (tab.isSelected()) {
                mainWindowController.setContentToSnippetTagsList((CodeSnippet) object);
            }
        });
        mainWindowController.getTabPaneWithEditSnippets().getTabs().add(tab);
        mainWindowController.getTabPaneWithEditSnippets().getSelectionModel().select(tab);
    }

    private boolean isTabOpening(String tabTitle) {
        for (Tab tab : mainWindowController.getTabPaneWithEditSnippets().getTabs()) {
            if (tab.getText().equals(tabTitle)) {
                return true;
            }
        }
        return false;
    }

    public Tab getTabByName(String tabTitle) {
        for (Tab tab : mainWindowController.getTabPaneWithEditSnippets().getTabs()) {
            if (tab.getText().equals(tabTitle)) {
                return tab;
            }
        }
        return null;
    }

    public void setMainController(MainWindowController mainWindowController) {
        this.mainWindowController = mainWindowController;
    }
}
