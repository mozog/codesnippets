package utils;

import java.util.ArrayList;
import java.util.List;

public class SnippetsInfo {

    private static List<String> listProgrammingLanguage = new ArrayList<>();
    private static List<String> listTypeSnippet = new ArrayList<>();
    private static List<String> listLocationSnippet = new ArrayList<>();

    public List<String> getListLocationSnippet() {
        return listLocationSnippet;
    }

    public void setListLocationSnippet(List<String> listLocationSnippet) {
        SnippetsInfo.listLocationSnippet = listLocationSnippet;
    }

    public void setListProgrammingLanguage(List<String> listProgrammingLanguage) {
        SnippetsInfo.listProgrammingLanguage = listProgrammingLanguage;
    }

    public void setListTypeSnippet(List<String> listTypeSnippet) {
        SnippetsInfo.listTypeSnippet = listTypeSnippet;
    }

    public List<String>  getListProgrammingLanguage() {
        return listProgrammingLanguage;
    }

    public List<String> getListTypeSnipet() {
        return listTypeSnippet;
    }

    public void addProgrammingLanguageToList(String programmingLanguage) {
        if(!listProgrammingLanguage.contains(programmingLanguage) && programmingLanguage!=null){
            listProgrammingLanguage.add(programmingLanguage);
        }
    }

    public void addTypeToList(String type) {
        if(!listTypeSnippet.contains(type)){
            listTypeSnippet.add(type);
        }
    }

    public void addLocationToList(String location){
        if(!listLocationSnippet.contains(location)){
            listLocationSnippet.add(location);
        }
    }
}
