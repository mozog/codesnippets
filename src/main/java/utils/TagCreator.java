package utils;

import models.CodeSnippet;
import models.Tag;

import java.util.Date;
import java.util.Objects;

public class TagCreator {

    public Tag createOrFindInDBTag(String tagName) {
        if (Tags.isNewTag(tagName)) {
            Tag tag = new Tag(tagName, new Date());
            Tags.addTagToTagsList(tag);
            return tag;
        }
        return null;
    }

    public void addDependencyToTag(Tag tag, CodeSnippet codeSnippetWithTag){
        codeSnippetWithTag.getTags().add(tag);
        Objects.requireNonNull(tag).getCodeSnippets().add(codeSnippetWithTag);
    }
}
