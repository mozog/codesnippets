package utils;

import models.Tag;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Tags {

    private static List<Tag> listTags = new ArrayList<>();

    public static boolean isNewTag(String tagName){
        for(Tag tag:listTags){
            if(tag.getName().equals(tagName)){
                return false;
            }
        }
        return true;
    }

    public static Tag getTagByName(String nameTag){
        for(Tag tag:listTags){
            if(tag.getName().equals(nameTag)){
                return tag;
            }
        }
        return null;
    }

    public static void setListTags(List<Tag> listTags) {
        Tags.listTags = listTags;
    }

    public static List<Tag> getListTags() {
        return listTags;
    }

    public static void addTagToTagsList(Tag tag){
        listTags.add(tag);
    }

}
