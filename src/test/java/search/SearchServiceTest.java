package search;

import models.CodeSnippet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import services.SnippetServiceDB;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class SearchServiceTest {

    SearchService searchService = new SearchService();

    @Mock
    SnippetServiceDB snippetServiceDB = new SnippetServiceDB();

    CodeSnippet testSnippet, testSnippet2;

    @Before
    public void setUp() throws IOException {
        testSnippet = new CodeSnippet("TestSnippet", "some content", true, "", "me", "Java", "function", new Date());
        testSnippet2 = new CodeSnippet("TestSnippet2", "some content 2", false, "", "you", "Java", "function", new Date());
    }


    @Test
    public void findSnippetsWithParameterList() throws IOException {
        snippetServiceDB.saveSnippetToDB(testSnippet);
        snippetServiceDB.saveSnippetToDB(testSnippet2);
        List<SearchParameters> parametersList = new ArrayList<>();
        SearchParameters searchParameters = new SearchParameters();
        searchParameters.addSnippetName("TestSnippet2");
        searchParameters.addContents("some content 2");
        parametersList.add(searchParameters);
        List<CodeSnippet> searchedSnippets = searchService.findSnippetsWithParameterList(parametersList);
        System.out.println(searchedSnippets);
        Assert.assertEquals(1, searchedSnippets.size());
        snippetServiceDB.deleteSnippetFromDB(testSnippet);
        snippetServiceDB.deleteSnippetFromDB(testSnippet2);
    }

    @Test
    public void getSnippetsWithParameters() {
        snippetServiceDB.saveSnippetToDB(testSnippet);
        snippetServiceDB.saveSnippetToDB(testSnippet2);
        SearchParameters searchParameters = new SearchParameters();
        searchParameters.addSnippetName("Test");
        searchParameters.addContents("some");
        List<CodeSnippet> searchedSnippets = searchService.getSnippetsWithParameters(searchParameters);
        Assert.assertEquals(2, searchedSnippets.size());
        snippetServiceDB.deleteSnippetFromDB(testSnippet);
        snippetServiceDB.deleteSnippetFromDB(testSnippet2);
    }
}