package services;

import models.CodeSnippet;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import utils.ManagerDB;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class SnippetServiceDBTest {

    @Mock
    SnippetServiceDB snippetServiceDB = new SnippetServiceDB();

    @Mock
    ManagerDB managerDB = new ManagerDB();

    CodeSnippet testSnippet, testSnippet2;

    @Before
    public void setUp() throws IOException {
        testSnippet = new CodeSnippet("TestSnippet", "some content", true, "", "me", "Java", "function", new Date());
        testSnippet2 = new CodeSnippet("TestSnippet2", "some content 2", false, "", "you", "Java", "function", new Date());
    }

    @Test
    public void saveSnippetToDB() {

        snippetServiceDB.saveSnippetToDB(testSnippet);
        managerDB.loadDateFromDB();
        boolean isSaved = SnippetServiceDB.getCodeSnippetList().contains(testSnippet);
        Assert.assertTrue(isSaved);
        snippetServiceDB.deleteSnippetFromDB(testSnippet);

    }

    @Test
    public void deleteSnippetFromDB() {

        snippetServiceDB.saveSnippetToDB(testSnippet);
        managerDB.loadDateFromDB();
        boolean isSaved = SnippetServiceDB.getCodeSnippetList().contains(testSnippet);
        Assert.assertTrue(isSaved);
        snippetServiceDB.deleteSnippetFromDB(testSnippet);
        managerDB.loadDateFromDB();
        boolean isOnList = SnippetServiceDB.getCodeSnippetList().contains(testSnippet);
        Assert.assertFalse(isOnList);

    }
}